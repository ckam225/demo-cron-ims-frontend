import Vue from 'vue';
import VueRouter from 'vue-router';

import access from './middleware/access'
import guest from './middleware/guest';

import AuthGuest from './pages/auth/Guest';

import Error401 from './pages/errors/401.vue';
import Error404 from './pages/errors/404.vue';

import Frame from './pages/Frame'
import Home from './pages/Index'
import Return from './pages/Return.vue';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Frame,
      children: [
        {
          path: '',
          name: 'Home',
          component: Home,
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: 'return/:id',
          name: 'PosReturn',
          component: Return,
          meta: {
            middleware: [guest, access]
          }
        },
      ]
    },
    {
      path: 'guest',
      name: 'Guest',
      component: AuthGuest,
    },
    {
      path: '*',
      name: 'Error404',
      component: Error404,
    },
    {
      path: '/401',
      name: 'Error401',
      component: Error401
    },
  ]
});

function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];
  if (!subsequentMiddleware) return context.next;
  return (...parameters) => {
    context.next(...parameters);
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({...context, next: nextMiddleware});
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware) ? to.meta.middleware : [to.meta.middleware];
    const context = {from, next, router, to};
    const nextMiddleware = nextFactory(context, middleware, 1);
    return middleware[0]({...context, next: nextMiddleware});
  }
  return next();
});

export default router
