import VeeValidate, {Validator} from 'vee-validate';
import Vue from 'vue';
import VModal from 'vue-js-modal';
import VueNoty from 'vuejs-noty';

var createHost = require('cross-domain-storage/host');

import App from './App';
import defaultLayout from './layouts/Default';
import Utils from './plugins/utils';
import router from './router';
import store from './store';


Vue.component('default-layout', defaultLayout);

Vue.config.productionTip = false;
Vue.prototype.$eventBus = new Vue();
Vue.use(VeeValidate, {inject: false});
Validator.localize('ru', require('vee-validate/dist/locale/ru'));
Vue.use(VModal);
Vue.use(VueNoty);
Vue.use(Utils);

createHost([
  {
    origin: `${process.env.VUE_APP_WWW_URL}`,
    allowedMethods: ['get', 'set', 'remove']
  }
]);

export default new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app');
