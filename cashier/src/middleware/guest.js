import storage from '@/plugins/storage';

export default function guest({next}) {
  if (!storage.existsAuthUser()) {
    window.location.replace(`${process.env.VUE_APP_WWW_URL}/login`);
  }
  return next();
}
