import storage from '@/plugins/storage'

export default function access({next,router}) {
  if (storage.existsAuthUser()) {
    const user = storage.getAuthUser();
    if (user.meta !== null && user.meta.current_access) {
      if (user.meta.current_access !== 2) {
        storage.reset();
        return router.push({ name: 'Error401' });
      }
    }
  }
  return next();
}
