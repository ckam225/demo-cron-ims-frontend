class Storage {

  static set(key, data){
    localStorage.setItem(key, JSON.stringify(data));
  }

  static get(key){
    if(!this.exists(key))
      return null;

    return JSON.parse(localStorage.getItem(key));
  }

  static remove(key){
    localStorage.removeItem(key);
  }

  static exists(key){
    if (localStorage.getItem(key) &&
      localStorage.getItem(key) !== 'undefined' &&
      localStorage.getItem(key) !== 'null'){
      return true;
    }
    return false;
  }

  static existsAuthUser() {
    if(this.exists('user') && 
      this.get('user').hasOwnProperty('token')){
      return true;
    }
    return false;
  }

  static getAuthUser() {
    return this.get('user');
  }

  static reset(){
    localStorage.clear(); 
  }


}

export default Storage;