import InputField from '@/components/elements/InputField';
import TextField from '@/components/elements/TextField';
import RefillCashboxModal from '@/components/modals/RefillCashboxModal';
import storage from '@/plugins/storage';
import {createLocalVue, shallowMount} from '@vue/test-utils';
import VeeValidate from 'vee-validate';
import VModal from 'vue-js-modal';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(VModal);
localVue.use(Vuex);
localVue.use(VeeValidate, {
  inject: false
});

const shift = JSON.parse('{"id":101,"cashbox":{"id":3,"balance":"100.00","name":"Касса 3","created_at":"2019-09-25T18:01:43.622476+03:00","shop":{"id":2,"name":"Магазин 2","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Шамиля ","country":"Россия"},"phone":"8  555 400 40 40","created_by":2},"created_by":3},"number":92,"revenue":"100.00","opened_at":"2019-10-07T11:45:18.574015+03:00","closed_at":null,"created_by":4}');

const store = new Vuex.Store({
  mutations: {
    UPDATE_SHIFT: (state, [amount, is_insertion]) => {
      if (is_insertion) {
        state.shift.revenue = parseFloat(state.shift.revenue) + parseFloat(amount);
      } else {
        state.shift.revenue = parseFloat(state.shift.revenue) - parseFloat(amount);
      }
      storage.set('shift', state.shift);
    },
  },
  state: {
    shift
  },
  getters: {
    shift: state => state.shift
  }
});

describe('test RefillCashboxModal', () => {
  const wrapper = shallowMount(RefillCashboxModal, {
    data() {
      return {
        title: 'Пополнение кассы',
        btnText: 'Пополнить деньги',
        action: 0
      }
    },
    localVue,
    store,
    stubs: {
      InputField,
      TextField
    }
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('revenue value is 100', () => {
    expect(parseInt(store.getters.shift.revenue)).toBe(100);
  });

  it('add money with value 150', () => {
    const input = wrapper.find('.input');
    input.setValue(150);

    const textarea = wrapper.find('textarea');
    textarea.setValue('add money');
    wrapper.setData({action: 0});
    wrapper.setMethods({submit: () => store.commit('UPDATE_SHIFT', [150, true])});
    wrapper.find('form').trigger('submit.prevent');
    expect(store.state.shift.revenue).toBe(250);
  });

  it('remove money with value 40', () => {
    const input = wrapper.find('.input');
    input.setValue(40);

    const textarea = wrapper.find('textarea');
    textarea.setValue('remove money');
    wrapper.setData({action: 1});
    wrapper.setMethods({submit: () => store.commit('UPDATE_SHIFT', [40, false])});
    wrapper.find('form').trigger('submit.prevent');
    expect(store.state.shift.revenue).toBe(210);
  });

});
