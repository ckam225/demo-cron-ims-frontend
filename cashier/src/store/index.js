import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import clients from './modules/clients'
import employees from './modules/employees'
import products from './modules/products'
import returns from './modules/returns'
import sales from './modules/sales'
import shops from './modules/shops'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    clients,
    employees,
    products,
    returns,
    sales,
    shops
  },
  strict: process.env.NODE_ENV !== 'production'
});
