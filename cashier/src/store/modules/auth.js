import api from '@/api';
import storage from '@/plugins/storage';

export default {
  state: {
    user: {
      token: null,
      meta: null,
    }
  },
  mutations: {
    SET_USER_META(state, payload) {
      state.user.meta = payload.meta;
      storage.set('user', state.user)
    },
    SET_AUTH_OUT() {
      storage.reset();
    },
  },
  actions: {
    async me({commit}, id) {
      try {
        const response = await api.get(`/auth/users/${id}/`);
        commit('SET_USER_META', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async logout({commit}) {
      try {
        const response = await api.post(`/auth/token/logout/`);
        if (response.status !== 404) {
          commit('SET_AUTH_OUT');
        }
        return response
      } catch (error) {
        return error.response
      }
    }
  },
  getters: {
    isLoggedIn: state => !!state.user.token,
    user: state => state.user,
  }
}
