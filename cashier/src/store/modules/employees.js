import api from '@/api';


export default {
  state: {
    employees: [],
    positions: [],
    works: [],
    filteredEmployees: []
  },
  mutations: {
    SET_EMPLOYEES: (state, payload) => {
      state.employees = payload;
      state.filteredEmployees = [...payload];
    },
    ADD_EMPLOYEE: (state, payload) => {
      state.employees.push(payload);
      state.filteredEmployees.push(payload)
    },
    SET_POSITIONS: (state, payload) => state.positions = payload,
    ADD_POSITION: (state, payload) => state.positions.push(payload),
    SET_WORKS: (state, payload) => state.works = payload,
    ADD_WORK: (state, payload) => state.works.push(payload),
    SEARCH_EMPLOYEE_BY_SHOP: (state, shopId) => {
      state.filteredEmployees = state.employees;
      if (shopId) {
        state.filteredEmployees = state.filteredEmployees.filter((emp) => {
          if (emp.current_work) {
            return emp.current_work.shop === parseInt(shopId);
          } else {
            return false;
          }
        })
      }

    },
  },
  actions: {
    async getEmployees({commit}) {
      try {
        const response = await api.get('/employees/');
        commit('SET_EMPLOYEES', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async getEmployeeById(context, id ) {
      try {
        const response = await api.get(`/employees/${id}/`);
        return response
      } catch (e) {
        return e.response
      }
    },
    async addEmployee({commit}, form) {
      try {
        const response = await api.post('/employees/', form);
        commit('ADD_EMPLOYEE', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async deleteEmployee({dispatch}, id) {
      try {
        const response = await api.delete(`/employees/${id}/`);
        dispatch('getEmployees');
        return response
      } catch (e) {
        return e.response
      }
    },
    async getPositions({commit}) {
      try {
        const response = await api.get('/positions/');
        commit('SET_POSITIONS', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async getPositionById(context, id) {
      try {
        const response = await api.get(`/positions/${id}/`);
        return response
      } catch (e) {
        return e.response
      }
    },
    async addPosition({commit}, payload) {
      try {
        const response = await api.post('/positions/', payload);
        commit('ADD_POSITION', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async updatePositionEmployee({dispatch}, payload){
      try{
        const response = await api.put(`/positions/${payload.id}/`, payload.form);
        dispatch('getPositions');
        return response;
      }catch (e) {
        return e.response;
      }
    },
    async deletePosition({dispatch}, id){
      try{
        const response = await api.delete(`/positions/${id}/`);
        dispatch('getPositions');
        return response;
      }catch (e) {
        return e.response;
      }
    },
    async getWorks({commit}) {
      try {
        const response = await api.get('/works/');
        commit('SET_WORKS', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async addWork({commit}, form) {
      try {
        const response = await api.post('/works/', form);
        commit('ADD_WORK', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async searchEmployeeByShop({commit}, shopId) {
      commit('SEARCH_EMPLOYEE_BY_SHOP', shopId)
    },
    async updateEmployee(context, employee){
      try{
        const response = await api.put(`/employees/${employee.id}/`, employee);
        return response;
      }catch (e) {
        return e.response;
      }
    }
  },
  getters: {
    employees: (state) => state.employees,
    filteredEmployees: (state) => state.filteredEmployees,
    positions: (state) => state.positions,
    works: (state) => state.works,
    getEmployeeById: state => id => state.employees.find(employee => employee.user.id === id),
  }
}
