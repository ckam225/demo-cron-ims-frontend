/* eslint-disable */
import api from '@/api';
import Vue from 'vue'

function setRangeReturns(returns) {
  const ranges = []
  returns.forEach((retrn) => {
    const date = new Vue().$date.toDateFormat(retrn.created_at)
    const range = ranges.find(r => r.date == date)
    if(range != null){
      range.entries.push(retrn)
    }else{
      ranges.push({date: new Vue().$date.toDateFormat(retrn.created_at), entries: [retrn]})
    }
  })
  return ranges
}

export default {
  state: {
   returns: [],
   rangeReturns: [],
  },
  mutations: {
    SET_RETURNS: (state, payload) => {
      state.returns = payload
      state.rangeReturns = setRangeReturns(payload)
    },
    ADD_RETURN: (state, payload) => {
      state.returns.push(payload)
      state.rangeReturns = setRangeReturns(state.returns)
    },
    SEARCH_RETURNS: (state, payload) => {
      state.rangeReturns = setRangeReturns(payload)
    },
  },
  actions: {
    async fetchReturns({commit}){
      try {
        const response = await api.get(`/returns/`)
        commit("SET_RETURNS", response.data)
        return response
      } catch (e) {
        return e.response
      }
    },
    async getReturn(context, id){
      try {
        const response = await api.get(`/returns/${id}`)
        return response
      } catch (e) {
        return e.response
      }
    },
    async deleteReturn(context, id){
      try {
        const response = await api.delete(`/returns/${id}`)
        return response
      } catch (e) {
        return e.response
      }
    },
    async searchReturns({commit}, searchParams){
      try {
        const response = await api.get(`/returns/`, {params: searchParams})
        commit("SEARCH_RETURNS", response.data)
        return response
      } catch (e) {
        return e.response
      }
    },
    async deleteReturnLine(context, id){
      try {
        const response = await api.delete(`/returnlines/${id}`)
        return response
      } catch (e) {
        return e.response
      }
    }
  },
  getters: {
    returns: (state) => state.returns,
    rangeReturns: (state) => state.rangeReturns,
  }
}
