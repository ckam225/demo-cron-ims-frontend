import api from '@/api';
import storage from '@/plugins/storage';

export default {
  state: {
    clients: [],
    currentClient:  storage.get('client'),
    defaultRetailClient: storage.get('default-retail-client')
  },
  mutations: {
    SET_CLIENTS: (state, payload) => state.clients = payload,
    ADD_CLIENT: (state, payload) => state.clients.push(payload),
    SET_DEFAULT_RETAIL_CLIENT: (state, payload) => {
      storage.set('default-retail-client', payload);
      state.defaultRetailClient = payload;
    },
    SET_CURRENT_CLIENT: (state, payload) => {
      storage.set('client', payload);
      state.currentClient = payload;
    }
  },
  actions: {
    async getListClients({commit}) {
      try {
        const response = await api.get(`/clients/`);
        commit('SET_CLIENTS', response.data);
        commit('SET_DEFAULT_RETAIL_CLIENT', response.data[0]);
        if (!this.currentClient)
          commit('SET_CURRENT_CLIENT', response.data[0]);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async createClient({commit}, form) {
      try {
        const response = await api.post(`/clients/`, form);
        commit('ADD_CLIENT', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async searchClient({commit}, payload) {
      try {
        const response = await api.get(`/clients/`, {
          params:{
            search: payload
          }
         });
        commit('SET_CLIENTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async deleteClient({dispatch}, id){
      try {
        const response = await api.delete(`/clients/${id}/`);
        dispatch('getListClients');
        return response;
      } catch (e) {
        return e.response;
      }
    }
  },
  getters: {
    clients: (state) => state.clients,
    currentClient: (state) => state.currentClient,
    defaultRetailClient: (state) => state.defaultRetailClient
  }
};
