import api from '@/api';
import Vue from 'vue';
import storage from '@/plugins/storage';

function deleteEmptyChildren(categories) {
  const lst = categories.slice();
  for (let i = 0; i < lst.length; i++) {
    if (lst[i].children.length > 0) {
      deleteEmptyChildren(lst[i].children)
    } else {
      delete lst[i].children
    }
  }
  return lst
}

function calculateSumCart(cart) {
  let sum = 0;
  let sumDiscount = 0;
  const client = storage.get('client');
  cart.forEach(function (el) {
    const price = (client !== null && client.is_retail) ? el.price.retail : el.price.wholesale;
    sum = sum + Number(el.qte) * parseFloat(price);
    sumDiscount = el.discount > 0 ? sum - (sum * el.discount/100) : 0;
  });
  return {sum: sum, sumDiscount: sumDiscount}
}


function filterCategories(categories) {
  const arr = [];
  categories.forEach(function (el) {
    if (!el.parent) {
      arr.push(el)
    }
  });
  return arr
}


export default {
  state: {
    products: [],
    categories: [],
    cart: [],
    sumCart: 0,
    sumDiscountCart: 0,
    categories_filtered: [],
  },
  mutations: {
    SET_PRODUCTS: (state, payload) => {
      state.products = payload;
    },
    UPDATE_PRODUCT_BALANCE: (state, payload) => {
      const product = state.products.find(p => p.id === payload.product);
      product.balance -= payload.count;
    },
    SET_CATEGORIES: (state, payload) => {
      state.categories = payload;
      state.categories_filtered = filterCategories(payload)
    },
    ADD_TO_CART: (state, payload) => {
      if (state.currentCategory !== null) {
        const isService = payload.product_type === 2;
        if (!isService && !payload.balance) {
          return
        }
        const product = state.cart.find(p => p.id === payload.id);
        if (product) {
          if (isService) return;
          if (product.qte < product.balance) {
            product.qte++;
          }
        } else {
          state.cart.push({...payload, qte: 1})
        }
        const sumCalc = calculateSumCart(state.cart);
        state.sumCart = sumCalc.sum;
        state.sumDiscountCart = sumCalc.sumDiscount;
      } else {
        new Vue().$toast.error("Пожалуйста, выберите клиента!")
      }
    },
    UPDATE_CART: (state, payload) => {
      const product = state.cart.find(p => p.id === payload.id);
      if (product && payload.qte <= product.balance) {
        product['qte'] = payload.qte
      }
      const sumCalc = calculateSumCart(state.cart);
      state.sumCart = sumCalc.sum;
      state.sumDiscountCart = sumCalc.sumDiscount;
    },
    REMOVE_FROM_CART: (state, payload) => {
      const product = state.cart.find(p => p.id === payload.id);
      if (product) {
        state.cart.splice(payload.index, 1)
      }
      const sumCalc = calculateSumCart(state.cart);
      state.sumCart = sumCalc.sum;
      state.sumDiscountCart = sumCalc.sumDiscount;
    },
    TRUNCATE_CART: (state) => {
      state.cart = [];
      state.sumCart = 0;
      state.sumDiscountCart = 0;
    },
    FILTER_CATEGORIES: (state, payload) => {
      const categories = state.categories.find(p => p.id === payload.id);
      if (categories.children) {
        state.categories_filtered = categories.children
      } else {
        state.categories_filtered = [];
      }
    },
    RESTORE_CATEGORIES: (state) => {
      state.categories_filtered = filterCategories(state.categories)
    }
  },
  actions: {
    filterCategories({commit}, payload){
      commit('FILTER_CATEGORIES', payload)
    },
    restoreCategories({commit}){
      commit('RESTORE_CATEGORIES')
    },
    async fetchProducts({commit}, id) {
      if(id === null) id ='null';
      const config = {
        params: {
          category: id
        }
      }
      try {
        const response = await api.get(`/products/`, config);
        commit('SET_PRODUCTS', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async fetchProductsByShop({commit}) {
      try {
        const shift = storage.get('shift');
        if (!shift) return false;
        const response = await api.get(`/products/?shop=${shift.cashbox.shop.id}`);
        commit('SET_PRODUCTS', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async fetchCategories({commit}, id) {
      const config = {};
      if (id) {
        config.params = {
          parent: id
        }
      }
      try {
        const response = await api.get(`/categories/`, config);
        commit('SET_CATEGORIES', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async submitNewProduct({dispatch}, payload) {
      const config = {
        headers: {'Content-Type': 'multipart/form-data'}
      };

      try {
        const response = await api.post(`/products/`, payload.formData, config);
        dispatch('fetchProducts', payload.currentCategory);
        new Vue().$eventBus.$emit('added-new-product');
        return response;
      } catch (e) {
        return e.response
      }
    },
    async submitNewFolder({dispatch}, payload) {
      try {
        const response = await api.post(`/categories/`, payload);
        dispatch('fetchCategories');
        return response;
      } catch (e) {
        return e.response
      }
    },
    async updateCategory({dispatch}, payload) {
      try {
        const response = await api.put(`/categories/${payload.id}/`, payload.form);
        dispatch('fetchCategories');
        return response
      } catch (e) {
        return e.response
      }
    },

    async deleteCategory({dispatch}, {id, parentCategory}) {
      try {
        const response = await api.delete(`/categories/${id}/`);
        dispatch('fetchCategories');
        dispatch('fetchProducts', parentCategory);
        return response
      } catch (e) {
        return e.response
      }
    },
    async updateProduct({dispatch}, payload) {
      try {
        const response = await api.put(`/products/${payload.id}/`, payload.form);
        dispatch('fetchProducts', payload.parentCategory);
        return response
      } catch (e) {
        return e.response
      }
    },
    async deleteProduct({dispatch}, payload) {
      try {
        const response = await api.delete(`/products/${payload.id}/`);
        dispatch('fetchProducts', payload.categoryId);
        return response
      } catch (e) {
        return e.response
      }
    },
    async searchProduct({commit}, searchText) {
      const config = {
        params: {
          name__icontains: searchText
        }
      };
      try {
        const response = await api.get(`/products/`, config);
        commit('SET_PRODUCTS', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async addProductToCart({commit}, product) {
      commit('ADD_TO_CART', product);
    },
    async generateBarcode() {
      try {
        return await api.get(`/barcode`)
      } catch (e) {
        return e.response
      }
    },
    async searchProductBy({commit}, payload) {
      try {
        const response = await api.get(`/products/`, {params: payload});
        if(payload.barcode && response.data.length === 1){
          commit('ADD_TO_CART', response.data[0]);
        }
        commit('SET_PRODUCTS', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
    async searchProductsByCategory({commit}, payload) {
      try {
        const response = await api.get(`/products/`, {
          params: {
            shop: payload.shopId,
            category: payload.category.id,
            product_type: payload.category.product_type
          }
        });
        commit('SET_PRODUCTS', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
  },
  getters: {
    products: ({products}) => products,
    categories: ({categories}) => categories,
    categoriesWithoutEmptyChildren: ({categories}) => deleteEmptyChildren(categories),
    cart: ({cart}) => cart,
    sumCart: ({sumCart}) => sumCart,
    sumDiscountCart: ({sumDiscountCart}) => sumDiscountCart,
    categories_filtered: ({categories_filtered}) => categories_filtered
  }
};
