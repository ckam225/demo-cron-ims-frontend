import storage from '@/plugins/storage'

export default function accessManager({next}) {
  if (storage.exists('user')) {
    const user = storage.getAuthUser();
    if (user.meta !== null && user.meta.current_access) {
      if (user.meta.current_access !== 1) {
        location.href = `${process.env.VUE_APP_POS_URL}`;
        storage.reset();
      }
    }
  }
  return next();
}
