import storage from '@/plugins/storage'

export default function guest({next, router}) {
  if (!storage.exists('user')) {
    return router.push({name: 'Login'});
  }
  return next();
}
