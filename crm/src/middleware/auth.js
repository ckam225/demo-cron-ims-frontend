import storage from '@/plugins/storage'

export default function auth({ next, router }) {
  if (storage.exists('user')) {
    return router.push({ name: 'Home' });
  }
  return next();
}
