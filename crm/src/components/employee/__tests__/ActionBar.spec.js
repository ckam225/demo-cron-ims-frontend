import {createLocalVue, mount} from "@vue/test-utils";
import ActionBar from "@/components/employee/ActionBar";
import ActionButton from "@/components/elements/ActionButton";
import VModal from "vue-js-modal";
import VeeValidate from "vee-validate";

const localVue = createLocalVue();

localVue.use(VModal);
localVue.use(VeeValidate, {
  inject: false
});

const shopList = JSON.parse('[{"id":8,"name":"Магазин 2","address":{"city":"Махачкала ","region":"Дагестан","street":"Шамиля","country":"Россия"},"phone":"8 800 400 40 40","created_by":16},{"id":7,"name":"Магазин 1","address":{"city":"Махачкала ","region":"Дагестан","street":"Ирчи Казака 11 а","country":"Россия"},"phone":"8 800 500 50 50","created_by":16}]');
const positionList = JSON.parse('[{"id":12,"name":"Кассир","created_by":16},{"id":11,"name":"Менеджер","created_by":16},{"id":10,"name":"Директор","created_by":16},{"id":9,"name":"Управляющий","created_by":16}]');

describe('test EmployeesActionBar', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(ActionBar, {
      localVue,
      propsData: {
        shopList,
        positionList
      }
    })
  });

  it('has class `.action-bar` ', () => {
    expect(wrapper.contains('.action-bar')).toBeTruthy();
  });

  it('test method searchChanged', () => {
    const events = {};
    const $emit = (event, ...args) => { events[event] = [...args] };

    ActionBar.methods.searchChanged.call({ $emit, search: 'searchText'});
    expect(events['search-changed']).toEqual(['searchText']);
  });

  it('test method shop-changed', () => {
    const events = {};
    const $emit = (event, ...args) => { events[event] = [...args] };

    ActionBar.methods.chopChanged.call({ $emit, shop: 'shopName'});
    expect(events['shop-changed']).toEqual(['shopName']);
  });

  it('show modal create-employee', () => {
    wrapper.find(ActionButton).trigger('click');
    expect(wrapper.contains('[data-modal="create-employee"]')).toBeTruthy();
  });

});

