import {mount, RouterLinkStub, createLocalVue} from '@vue/test-utils';
import EmployeesTable from '@/components/employee/Table';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const positions = JSON.parse('[{"id":12,"name":"Кассир","created_by":16},{"id":11,"name":"Менеджер","created_by":16},{"id":10,"name":"Директор","created_by":16},{"id":9,"name":"Управляющий","created_by":16}]');
const shops = JSON.parse('[{"id":8,"name":"Магазин 2","address":{"city":"Махачкала ","region":"Дагестан","street":"Шамиля","country":"Россия"},"phone":"8 800 400 40 40","created_by":16},{"id":7,"name":"Магазин 1","address":{"city":"Махачкала ","region":"Дагестан","street":"Ирчи Казака 11 а","country":"Россия"},"phone":"8 800 500 50 50","created_by":16}]');


const store = new Vuex.Store({
  getters: {
    shops(){
      return shops
    },
    positions(){
      return positions
    }
  }
})

const entries = JSON.parse('[{"id":18,"current_work":{"id":18,"shop":8,"position":10,"is_cashier":1,"salary":90000},"user":{"id":20,"email":"director@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:29:58.323709+03:00"},"name":"Магомедов Магомед Магомедович","phone":"+7 999 999 99 99","gender":"MALE","document":{"number":"487161563","serial":"4566","issued_by":"Россия"},"birthday":"2000-10-01","created_at":"2019-10-30T16:29:58.525012+03:00","created_by":16},{"id":17,"current_work":{"id":17,"shop":8,"position":12,"is_cashier":2,"salary":50000},"user":{"id":19,"email":"cashier2@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:28:36.050926+03:00"},"name":"Сергеева Инна Вячеславовна","phone":"+7 888 800 80 80","gender":"FEMALE","document":{"number":"3154851","serial":"7974","issued_by":"Россия"},"birthday":"2000-07-09","created_at":"2019-10-30T16:28:36.258738+03:00","created_by":16},{"id":16,"current_work":{"id":16,"shop":7,"position":12,"is_cashier":2,"salary":50000},"user":{"id":18,"email":"cashier@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:27:19.200017+03:00"},"name":"Ахмедханова Фаина Исламовна","phone":"+7 777 456 78 90","gender":"FEMALE","document":{"number":"474575","serial":"7847","issued_by":"Россия"},"birthday":"2000-10-01","created_at":"2019-10-30T16:27:19.425571+03:00","created_by":16},{"id":15,"current_work":{"id":15,"shop":7,"position":11,"is_cashier":1,"salary":70000},"user":{"id":17,"email":"manager@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:25:49.999117+03:00"},"name":"Ахмедов Ислам Мухтарович","phone":"+71234567890","gender":"MALE","document":{"number":"02","serial":"01","issued_by":"Россия"},"birthday":"2000-10-01","created_at":"2019-10-30T16:25:50.269215+03:00","created_by":16},{"id":14,"current_work":{"id":14,"shop":7,"position":9,"is_cashier":1,"salary":null},"user":{"id":16,"email":"cron@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:15:53.522027+03:00"},"name":"cron@mail.ru","phone":"","gender":"MALE","document":{},"birthday":null,"created_at":"2019-10-30T16:15:53.763740+03:00","created_by":16}]');

describe('test EmployeesTable', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(EmployeesTable, {
      localVue,
      store,
      propsData: {
        entries,
        loading: false
      },
      mocks: {
        $modal: {
          show: jest.fn()
        }
      },
      stubs: {
        RouterLink: RouterLinkStub,
        ProgressBar: "<div id='test-progress-bar'></div>"
      }
    });
  });

  it('not render progress-bar', () => {
    expect(wrapper.contains('#test-progress-bar')).toBeFalsy();
  });

  it('should be 5 tr in tbody', () => {
    expect(wrapper.element.querySelectorAll('tbody > tr')).toHaveLength(5);
  });

  it('render progress-bar', () => {
    wrapper.setProps({
      entries: [],
      loading: true
    });
    expect(wrapper.element.querySelectorAll('tbody > tr')).toHaveLength(1);
    expect(wrapper.contains('#test-progress-bar')).toBeTruthy();
  });

  it('called $modal.show', () => {
    wrapper.find('.btn--delete').trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalled();
  });

});
