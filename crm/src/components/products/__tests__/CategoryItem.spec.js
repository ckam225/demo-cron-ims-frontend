import ImageButton from '@/components/elements/ImageButton';
import CategoryItem from '@/components/products/CategoryItem';

import {mount} from '@vue/test-utils';


const category = JSON.parse('{"id":3,"name":"Компьютеры","category_type":1,"parent":null,"created_by":2,"children":[{"id":5,"name":"Видеокарты","category_type":1,"parent":3,"created_by":2,"children":[{"id":11,"name":"AMD","category_type":1,"parent":5,"created_by":2,"discount":0},{"id":10,"name":"Nvidia","category_type":1,"parent":5,"created_by":2,"discount":0}],"discount":0},{"id":9,"name":"Жесткие диски","category_type":1,"parent":3,"created_by":3,"discount":0},{"id":14,"name":"Материнские платы","category_type":1,"parent":3,"created_by":2,"discount":0},{"id":4,"name":"Процессоры","category_type":1,"parent":3,"created_by":2,"children":[{"id":12,"name":"AMD","category_type":1,"parent":4,"created_by":2,"discount":0},{"id":13,"name":"Intel","category_type":1,"parent":4,"created_by":2,"discount":0}],"discount":0}],"discount":0}');


describe('test CategoryItem', () => {

  const wrapper = mount(CategoryItem, {
    propsData: {
      category
    },
    stubs: {
      ImageButton,
    }
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('has selector .folder', () => {
    expect(wrapper.contains('.folder')).toBeTruthy();
  });

  it('has text `Компьютеры` ', () => {
    expect(wrapper.text()).toMatch(/Компьютеры/);
  });

});
