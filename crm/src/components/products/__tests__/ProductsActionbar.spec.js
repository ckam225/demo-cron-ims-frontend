import ActionButton from '@/components/elements/ActionButton';
import ProductsActionbar from '@/components/products/ProductsActionbar';

import {createLocalVue, mount} from '@vue/test-utils';
import VeeValidate from 'vee-validate';
import VModal from 'vue-js-modal';
import Vuex from 'vuex';


const localVue = createLocalVue();
localVue.use(VModal);
localVue.use(Vuex);
localVue.use(VeeValidate, {
  inject: false
});

const $route = {
  meta: {
    type: 1
  }
};

const store = new Vuex.Store({
  getters: {
    categories() { return [] },
  },
});


describe('test ProductList', () => {

  const wrapper = mount(ProductsActionbar, {
    store,
    localVue,
    propsData: {
      currentCategory: null
    },
    stubs: {
      ActionButton
    },
    mocks: {
      $route,
    }
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('no showed new-product modal', () => {
    expect(wrapper.contains('[data-modal="new-product"]')).toBeFalsy();
  });

  it('showed new-product modal', () => {
    const newProductButton =  wrapper.find('#test-new-product-btn');
    newProductButton.trigger('click');
    expect(wrapper.contains('[data-modal="new-product"]')).toBeTruthy();
  });

  it('no showed new-folder modal', () => {
    expect(wrapper.contains('[name="new-folder"]')).toBeFalsy();
  });

  it('showed new-folder modal', () => {
    const newProductButton =  wrapper.find('#test-new-folder-btn');
    newProductButton.trigger('click');
    expect(wrapper.contains('[data-modal="new-folder"]')).toBeTruthy();
  });

  it('to have been called search method', () => {
    wrapper.setMethods({ search: jest.fn() });
    const searchField = wrapper.find('.search-input');
    searchField.setValue('Ryzen');
    searchField.trigger('input');
    expect(wrapper.vm.search).toHaveBeenCalled();
  })

});
