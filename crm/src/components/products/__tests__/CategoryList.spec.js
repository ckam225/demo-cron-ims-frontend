import ImageButton from '@/components/elements/ImageButton';
import CategoryItem from '@/components/products/CategoryItem';
import CategoryList from '@/components/products/CategoryList';

import {createLocalVue, mount} from '@vue/test-utils';
import VeeValidate from 'vee-validate';
import VModal from 'vue-js-modal';
import Vuex from 'vuex';


const localVue = createLocalVue();
localVue.use(VModal);
localVue.use(Vuex);
localVue.use(VeeValidate, {
  inject: false
});

const categories = JSON.parse('[{"id":3,"name":"Компьютеры","category_type":1,"parent":null,"created_by":2,"children":[{"id":5,"name":"Видеокарты","category_type":1,"parent":3,"created_by":2,"children":[{"id":11,"name":"AMD","category_type":1,"parent":5,"created_by":2,"discount":0},{"id":10,"name":"Nvidia","category_type":1,"parent":5,"created_by":2,"discount":0}],"discount":0},{"id":9,"name":"Жесткие диски","category_type":1,"parent":3,"created_by":3,"discount":0},{"id":14,"name":"Материнские платы","category_type":1,"parent":3,"created_by":2,"discount":0},{"id":4,"name":"Процессоры","category_type":1,"parent":3,"created_by":2,"children":[{"id":12,"name":"AMD","category_type":1,"parent":4,"created_by":2,"discount":0},{"id":13,"name":"Intel","category_type":1,"parent":4,"created_by":2,"discount":0}],"discount":0}],"discount":0},{"id":16,"name":"Моноблоки","category_type":1,"parent":null,"created_by":3,"discount":0},{"id":1,"name":"Ноутбуки","category_type":1,"parent":null,"created_by":2,"discount":0},{"id":15,"name":"Планшеты","category_type":1,"parent":null,"created_by":3,"discount":0},{"id":2,"name":"Телефоны","category_type":1,"parent":null,"created_by":2,"discount":0}]');

const store = new Vuex.Store({
  getters: {
    categories() {
      return []
    },
  },
  actions: {
    updateCategory() {
      return {
        status: 200
      }
    }
  }
});


describe('test CategoryList', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = mount(CategoryList, {
      localVue,
      store,
      propsData: {
        categories,
        currentCategory: null
      },
      stubs: {
        ImageButton,
        CategoryItem
      }
    });
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should be 5 CategoryItem', () => {
    expect(wrapper.findAll(CategoryItem)).toHaveLength(5);
  });

  it('no called emits', () => {
    wrapper.setMethods({emits: jest.fn()});
    expect(wrapper.vm.emits).not.toHaveBeenCalled();
  });

  it('called emits', () => {
    wrapper.setMethods({emits: jest.fn()});
    const categoryItem = wrapper.find(CategoryItem);
    categoryItem.trigger('click');
    expect(wrapper.vm.emits).toHaveBeenCalled();
  });

  it('no showed modal UpdateCategory', () => {
    expect(wrapper.contains('[data-modal="update-category"]')).toBeFalsy();
  });

  it('showed modal UpdateCategory', () => {
    const btn = wrapper.find('.btn--edit');
    btn.trigger('click');
    expect(wrapper.contains('[data-modal="update-category"]')).toBeTruthy();
  });

});
