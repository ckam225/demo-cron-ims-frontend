import ImageButton from '@/components/elements/ImageButton';
import ProductItem from '@/components/products/ProductItem';
import ProductList from '@/components/products/ProductList';

import {createLocalVue, mount} from '@vue/test-utils';
import VeeValidate from 'vee-validate';
import VModal from 'vue-js-modal';
import Vuex from 'vuex';


const localVue = createLocalVue();
localVue.use(VModal);
localVue.use(Vuex);
localVue.use(VeeValidate, {
  inject: false
});

const products = JSON.parse('[{"id":34,"photos":[],"price":{"product":34,"purchases":"0.00","wholesale":"0.00","retail":"0.00","created_at":"2019-09-12T20:10:07.692615+03:00","updated_at":"2019-09-30T18:51:02.667018+03:00"},"discount":0,"name":"AMD Ryzen™ 3","product_type":1,"description":"ntfynf","barcode":"0730143309110","category":12,"created_by":3},{"id":36,"photos":[],"price":{"product":36,"purchases":"0.00","wholesale":"0.00","retail":"0.00","created_at":"2019-09-12T20:10:07.731646+03:00","updated_at":"2019-09-30T18:51:02.719689+03:00"},"discount":0,"name":"AMD Ryzen™ 5","product_type":1,"description":"AMD Ryzen™ 5","barcode":"","category":12,"created_by":3},{"id":35,"photos":[],"price":{"product":35,"purchases":"0.00","wholesale":"0.00","retail":"0.00","created_at":"2019-09-12T20:14:23.330252+03:00","updated_at":"2019-09-30T18:51:02.779420+03:00"},"discount":0,"name":"AMD Ryzen™ 7","product_type":1,"description":"","barcode":"","category":12,"created_by":3},{"id":33,"photos":[],"price":{"product":33,"purchases":"20.00","wholesale":"30.00","retail":"40.00","created_at":"2019-09-13T13:18:36.451317+03:00","updated_at":"2019-09-23T11:51:17.129378+03:00"},"discount":0,"name":"AMD Ryzen™ 9","product_type":1,"description":"","barcode":"","category":12,"created_by":3},{"id":37,"photos":[],"price":{"product":37,"purchases":"20.00","wholesale":"30.00","retail":"40.00","created_at":"2019-09-13T13:18:43.299739+03:00","updated_at":"2019-09-23T11:51:17.165831+03:00"},"discount":0,"name":"AMD Ryzen™ Threadripper™","product_type":1,"description":"","barcode":"","category":12,"created_by":3}, {"id":49,"photos":[],"price":{"product":49,"purchases":"0.00","wholesale":"500.00","retail":"500.00","created_at":"2019-09-13T13:49:30.413053+03:00","updated_at":"2019-09-25T14:13:20.531832+03:00"},"discount":0,"name":"Диагностика","product_type":2,"description":"Общая диагностика","barcode":"","category":8,"created_by":3}]');
const $route = {
  meta: {
    type: 1
  }
};

const store = new Vuex.Store({
  getters: {
    categories() {
      return []
    },
  },
  actions: {
    updateProduct() {
      return {
        status: 200
      }
    }
  }
});


describe('test ProductList', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = mount(ProductList, {
      localVue,
      store,
      propsData: {
        products,
      },
      stubs: {
        ImageButton,
        ProductItem
      },
      mocks: {
        $route
      }
    });
  });


  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should be 5 ProductItem', () => {
    expect(wrapper.findAll(ProductItem)).toHaveLength(5);
  });

  it('has text `AMD Ryzen™ 7`', () => {
    expect(wrapper.text()).toMatch(/AMD Ryzen™ 7/);
  });

  it('no has text `диагностика`', () => {
    expect(wrapper.text()).not.toMatch(/диагностика/);
  });

  it('no showed modal UpdateProduct', () => {
    expect(wrapper.contains('[data-modal="update-product"]')).toBeFalsy();
  });

  it('showed modal UpdateProduct', () => {
    const btn = wrapper.find('.btn--edit');
    btn.trigger('click');
    expect(wrapper.contains('[data-modal="update-product"]')).toBeTruthy();
  });

  it('should be products only with product_type: 1 ', () => {
    const localThis = {
      products,
      $route: {
        meta: {
          type: 1
        }
      }
    };
    const result = ProductList.computed.filterProducts.call(localThis);
    expect(result).toContainObject({product_type: 1});
    expect(result).not.toContainObject({product_type: 2});
  });

  it('should be products only with product_type: 2', () => {
    const localThis = {
      products,
      $route: {
        meta: {
          type: 2
        }
      }
    };
    const result = ProductList.computed.filterProducts.call(localThis);
    expect(result).toContainObject({product_type: 2});
    expect(result).not.toContainObject({product_type: 1});
  });

});


expect.extend({
  toContainObject(received, argument) {
    /**
     * проверяет, что у массива есть объект содержащий переданный key:value
     */

    const pass = this.equals(
      received,
      expect.arrayContaining([
        expect.objectContaining(argument)
      ])
    );

    if (pass) {
      return {
        message: () => (`expected ${this.utils.printReceived(received)} not to contain object ${this.utils.printExpected(argument)}`),
        pass: true
      }
    } else {
      return {
        message: () => (`expected ${this.utils.printReceived(received)} to contain object ${this.utils.printExpected(argument)}`),
        pass: false
      }
    }
  }
});
