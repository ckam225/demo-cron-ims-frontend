import ImageButton from '@/components/elements/ImageButton';
import ProductItem from '@/components/products/ProductItem';

import {mount} from '@vue/test-utils';


const product = JSON.parse('{"id":35,"photos":[],"price":{"product":35,"purchases":"0.00","wholesale":"0.00","retail":"0.00","created_at":"2019-09-12T20:14:23.330252+03:00","updated_at":"2019-09-30T18:51:02.779420+03:00"},"discount":0,"name":"AMD Ryzen™ 7","product_type":1,"description":"","barcode":"","category":12,"created_by":3}');


describe('test ProductItem', () => {

  const wrapper = mount(ProductItem, {
    propsData: {
      product
    },
    stubs: {
      ImageButton
    }
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('has selector .folder', () => {
    expect(wrapper.contains('.product')).toBeTruthy();
  });

  it('has text `AMD Ryzen™ 7`', () => {
    expect(wrapper.text()).toMatch(/AMD Ryzen™ 7/);
  });

});
