import '@/api';
import ProductsBreadcrumbs from '@/components/products/ProductsBreadcrumbs';
import productsStore from '@/store/modules/products.js';

import {createLocalVue, shallowMount} from '@vue/test-utils';
import Vuex from 'vuex';


const {mutations, getters} = productsStore;

const localVue = createLocalVue();
localVue.use(Vuex);

const breadcrumbs = JSON.parse('[{"id":3,"name":"Компьютеры"},{"id":5,"name":"Видеокарты"},{"id":11,"name":"AMD"}]');

const store = new Vuex.Store({
  state: {
    breadcrumbs
  },
  mutations,
  actions: {
    fetchProducts(){ return []; },
    fetchCategories(){ return []; }
  },
  getters,
});


describe('test removeBreadcrumbs', () => {

  const wrapper = shallowMount(ProductsBreadcrumbs, {
    store,
    localVue
  });
  wrapper.setMethods({
    fetchCategories: jest.fn(),
    toHome: jest.fn(),
    fetchProducts: jest.fn(),
    removeBreadcrumbs: jest.fn()
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('has selector .breadcrumbs', () => {
    expect(wrapper.is('.breadcrumbs')).toBeTruthy();
  });

  it('has text `Компьютеры` ', () => {
    expect(wrapper.text()).toMatch(/Компьютеры/);
  });

  it('called method toHome', () => {
    const item = wrapper.element.querySelector('.breadcrumbs-item:first-child .breadcrumbs-link');
    item.dispatchEvent(new Event('click'));
    expect(wrapper.vm.toHome).toHaveBeenCalled();
  });

  it('called method removeBreadcrumbs', () => {
    const item = wrapper.element.querySelector('.breadcrumbs-item:nth-child(2) span');
    item.dispatchEvent(new Event('click'));
    expect(wrapper.vm.removeBreadcrumbs).toHaveBeenCalled();
  });

});
