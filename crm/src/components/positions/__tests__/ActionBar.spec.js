import ActionBar from "@/components/positions/ActionBar";
import ActionButton from "@/components/elements/ActionButton";
import {mount} from "@vue/test-utils";

describe('test PositionsActionBar', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(ActionBar, {
      mocks: {
        $modal: {
          show: jest.fn()
        }
      }
    })
  });

  it('called $modal.show', () => {
    wrapper.find(ActionButton).trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalledWith('create-position');
  })
});
