import Table from '@/components/positions/Table';
import {mount} from "@vue/test-utils";

const employees = JSON.parse('[{"id":7,"current_work":{"id":7,"shop":4,"position":2,"is_cashier":2,"salary":12345},"user":{"id":9,"email":"kolbas-Iahmedov@example.com","is_superuser":false,"is_active":true,"date_joined":"2019-09-13T16:19:33+03:00"},"name":"Ахмедов Ислам Мухтарович","phone":"+71234567890","gender":"MALE","document":{"number":"123456","serial":"1234","issued_by":"МВД по Гунибскому району"},"birthday":"1999-09-05","created_at":"2019-09-13T16:19:33.275000+03:00","created_by":2},{"id":6,"current_work":{"id":6,"shop":3,"position":2,"is_cashier":2,"salary":12345},"user":{"id":8,"email":"kolbas-afaina@example.com","is_superuser":false,"is_active":true,"date_joined":"2019-09-13T16:17:25+03:00"},"name":"Ахмедханова Фаина Исламовна","phone":"+71234567890","gender":"FEMALE","document":{"number":"123456","serial":"1234","issued_by":"ОУФМС по Дахадаевскому району РД"},"birthday":"1996-02-01","created_at":"2019-09-13T16:17:25.640000+03:00","created_by":2},{"id":5,"current_work":{"id":5,"shop":3,"position":2,"is_cashier":2,"salary":12345},"user":{"id":7,"email":"kolbas-mmagomedov@example.com","is_superuser":false,"is_active":true,"date_joined":"2019-09-13T16:15:26+03:00"},"name":"Магомедов Магомед Магомед-гиреевич","phone":"+71234567890","gender":"MALE","document":{"number":"456789","serial":"1234","issued_by":"МВД по РД"},"birthday":"1998-12-01","created_at":"2019-09-13T16:15:26.942000+03:00","created_by":2},{"id":4,"current_work":{"id":4,"shop":3,"position":2,"is_cashier":2,"salary":12345},"user":{"id":6,"email":"kolbas-isergeeva@example.com","is_superuser":false,"is_active":true,"date_joined":"2019-09-13T16:13:35+03:00"},"name":"Сергеева Инна Вячеславовна","phone":"+71234567890","gender":"FEMALE","document":{"number":"567890","serial":"1234","issued_by":"ОУФМС по Ленинскому району г.Махачкала"},"birthday":"1995-03-12","created_at":"2019-09-13T16:13:35.988000+03:00","created_by":2},{"id":3,"current_work":{"id":3,"shop":4,"position":3,"is_cashier":1,"salary":12345},"user":{"id":5,"email":"kolbas-hipnash@example.com","is_superuser":false,"is_active":true,"date_joined":"2019-09-13T15:52:59+03:00"},"name":"Хаджимухаммедов Ислам-паша-Насретдин Ахмедхангаджиевич","phone":"+71234567890","gender":"MALE","document":{"number":"123456","serial":"1234","issued_by":"ОУФМС РФ по Республике Саха"},"birthday":"1990-06-01","created_at":"2019-09-13T15:52:59.783000+03:00","created_by":2},{"id":2,"current_work":{"id":2,"shop":3,"position":3,"is_cashier":1,"salary":12345},"user":{"id":4,"email":"kolbas-ivan@example.com","is_superuser":false,"is_active":true,"date_joined":"2019-09-13T15:48:44.372000+03:00"},"name":"Иванов Иван Иванович","phone":"+71234567890","gender":"MALE","document":{"number":"567890","serial":"1234","issued_by":"МВД по РД"},"birthday":"1985-01-01","created_at":"2019-09-13T15:48:44.567000+03:00","created_by":2},{"id":1,"current_work":{"id":1,"shop":null,"position":1,"is_cashier":1,"salary":null},"user":{"id":2,"email":"kolbas@example.com","is_superuser":false,"is_active":true,"date_joined":"2019-09-13T14:58:40.541000+03:00"},"name":"kolbas@example.com","phone":"","gender":"MALE","document":{},"birthday":null,"created_at":"2019-09-13T14:58:40.722000+03:00","created_by":2}]');
const positions = JSON.parse('[{"id":3,"name":"Заведующий Магазином","created_by":2},{"id":2,"name":"Кассир","created_by":2},{"id":1,"name":"Управляющий","created_by":2}]');

describe('test PositionsTable', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(Table, {
      computed: {
        employees(){
          return employees
        },
        positions(){
          return positions
        }
      },
      methods: {
        getPositions: jest.fn(),
        getEmployees: jest.fn(),
      },
      mocks: {
        $modal: {
          show: jest.fn()
        },
        $noty: {
          error: jest.fn()
        }
      }
    });
  });

  it('test method isUsedPosition with argument 3', () => {
    const localThis = { employees };
    const result = Table.methods.isUsedPosition.call(localThis, 3);

    expect(result).toBeTruthy();
  });

  it('test method isUsedPosition with argument 999', () => {
    const localThis = { employees };
    const result = Table.methods.isUsedPosition.call(localThis, 999);

    expect(result).toBeFalsy();
  });

  it('test method deletePosition', () => {
    const localThis = {
      isUsedPosition(){ return true },
      $noty: { error: jest.fn() }
    };
    Table.methods.deletePosition.call(localThis, {id: 3});
    expect(localThis.$noty.error).toHaveBeenCalledWith(
      'Данная должность закреплена за сотрудником', {layout: 'bottomRight'});
  });

  it('test method deletePosition', () => {
    const localThis = {
      isUsedPosition(){ return false },
      $modal: { show: jest.fn() }
    };
    const data = {id: 3};
    Table.methods.deletePosition.call(localThis, data);
    expect(localThis.$modal.show).toHaveBeenCalledWith('confirmation', data);
  });

  it('called $modal.show with argument update-position', () => {
    const data = {"created_by": 2, "id": 3, "name": "Заведующий Магазином"};
    wrapper.find('.btn--edit').trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalledWith('update-position', data);
  });

  it('called deletePosition', () => {
    wrapper.setMethods({ deletePosition: jest.fn() });
    wrapper.find('.btn--delete').trigger('click');
    expect(wrapper.vm.deletePosition).toHaveBeenCalled();
  });

});
