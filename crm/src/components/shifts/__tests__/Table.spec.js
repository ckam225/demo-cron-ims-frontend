import {mount, RouterLinkStub} from "@vue/test-utils";
import IndexTable from "@/components/shifts/Table";

const shifts = JSON.parse('[{"id":10,"cashbox":{"id":5,"name":"Касса","created_at":"2019-09-13T15:30:11.958000+03:00","shop":{"id":4,"name":"Колбасный Рай","address":{"city":"Каизляр","region":"Дагестан","street":"улица Колбас","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},"number":7,"revenue":"0.00","opened_at":"2019-09-20T14:19:52.094000+03:00","closed_at":"2019-09-20T14:19:58.907000+03:00","created_by":9},{"id":9,"cashbox":{"id":4,"name":"Касса 1.3","created_at":"2019-09-13T15:07:08.189000+03:00","shop":{"id":3,"name":"Мясной Маркет","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},"number":6,"revenue":"0.00","opened_at":"2019-09-20T14:01:19.693000+03:00","closed_at":null,"created_by":7},{"id":7,"cashbox":{"id":3,"name":"Касса 1.2","created_at":"2019-09-13T15:06:50.857000+03:00","shop":{"id":3,"name":"Мясной Маркет","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},"number":4,"revenue":"20613.00","opened_at":"2019-09-20T13:14:23.170000+03:00","closed_at":"2019-09-20T23:43:18.813000+03:00","created_by":6},{"id":8,"cashbox":{"id":4,"name":"Касса 1.3","created_at":"2019-09-13T15:07:08.189000+03:00","shop":{"id":3,"name":"Мясной Маркет","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},"number":5,"revenue":"3888.00","opened_at":"2019-09-20T08:47:58.938000+03:00","closed_at":"2019-09-20T14:01:19.403000+03:00","created_by":7},{"id":4,"cashbox":{"id":5,"name":"Касса","created_at":"2019-09-13T15:30:11.958000+03:00","shop":{"id":4,"name":"Колбасный Рай","address":{"city":"Каизляр","region":"Дагестан","street":"улица Колбас","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},"number":3,"revenue":"0.00","opened_at":"2019-09-19T15:47:22.388000+03:00","closed_at":"2019-09-20T14:19:51.818000+03:00","created_by":9},{"id":3,"cashbox":{"id":5,"name":"Касса","created_at":"2019-09-13T15:30:11.958000+03:00","shop":{"id":4,"name":"Колбасный Рай","address":{"city":"Каизляр","region":"Дагестан","street":"улица Колбас","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},"number":2,"revenue":"2032.25","opened_at":"2019-09-19T15:36:58.024000+03:00","closed_at":"2019-09-19T15:47:22.110000+03:00","created_by":9},{"id":2,"cashbox":{"id":2,"name":"Касса 1.1","created_at":"2019-09-13T15:01:57.534000+03:00","shop":{"id":3,"name":"Мясной Маркет","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},"number":1,"revenue":"18165.00","opened_at":"2019-09-19T15:26:50.402000+03:00","closed_at":"2019-09-19T15:32:14.281000+03:00","created_by":8}]');

describe('test ShiftsTable', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(IndexTable, {
      propsData: {
        shifts
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          getters: {
            getEmployeeById: jest.fn()
          }
        },
        $date: {
          toDateTime: jest.fn()
        }
      },
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
  });

  it('should be 7 tr in tbody', () => {
    expect(wrapper.element.querySelectorAll('tbody > tr')).toHaveLength(7);
  });

  it('not render tr in tbody', () => {
    wrapper.setProps({ shifts: [] });
    expect(wrapper.element.querySelectorAll('tbody > tr')).toHaveLength(0);
  })

});
