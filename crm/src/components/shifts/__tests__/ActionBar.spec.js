import {mount} from "@vue/test-utils";
import ActionBar from "@/components/shifts/ActionBar";

const cashboxes = JSON.parse('[{"id":5,"balance":null,"name":"Касса","created_at":"2019-09-13T15:30:11.958000+03:00","shop":{"id":4,"name":"Колбасный Рай","address":{"city":"Каизляр","region":"Дагестан","street":"улица Колбас","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},{"id":4,"balance":"0.00","name":"Касса 1.3","created_at":"2019-09-13T15:07:08.189000+03:00","shop":{"id":3,"name":"Мясной Маркет","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},{"id":3,"balance":null,"name":"Касса 1.2","created_at":"2019-09-13T15:06:50.857000+03:00","shop":{"id":3,"name":"Мясной Маркет","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2},{"id":2,"balance":null,"name":"Касса 1.1","created_at":"2019-09-13T15:01:57.534000+03:00","shop":{"id":3,"name":"Мясной Маркет","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2}]');
const shops = JSON.parse('[{"id":4,"name":"Колбасный Рай","address":{"city":"Каизляр","region":"Дагестан","street":"улица Колбас","country":"Россия"},"phone":"+71234567890","created_by":2},{"id":3,"name":"Мясной Маркет","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"phone":"+71234567890","created_by":2}]');


describe('test ShiftsActionBar', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(ActionBar, {
      computed: {
        shops() {
          return shops;
        },
        cashboxes(){
          return cashboxes;
        }
      },
      methods: {
        getListShops: jest.fn(),
        getCashboxes: jest.fn()
      }
    });
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should be class `.action-bar` ', () => {
    expect(wrapper.contains('.action-bar')).toBeTruthy();
  });

  it('test computed periodChanged', () => {
    const events = {};
    const $emit = (event, ...args) => { events[event] = [...args] };

    ActionBar.methods.periodChanged.call({ $emit}, 'text');
    expect(events['period-changed']).toEqual(['text']);
  });

  it('test computed changedCashbox', () => {
    const events = {};
    const $emit = (event, ...args) => { events[event] = [...args] };

    ActionBar.methods.changedCashbox.call({ $emit}, 1);
    expect(events['cashbox-changed']).toEqual([1]);
  });

  it('test computed method changedShop', () => {
    const events = {};
    const $emit = (event, ...args) => { events[event] = [...args] };

    ActionBar.methods.changedShop.call({
      $emit,
      cashbox: null,
      getCashboxesIds: 3,
      $refs: {
        select: {
          $el: {
            0: {
              selected: true
            }
          }
        }
      }
    });
    expect(events['shop-changed']).toEqual([3]);
  });

  it('test computed filterCashboxes with empty value shop', () => {
    const localThis = {
      shop: '',
      cashboxes,
    };
    expect(ActionBar.computed.filterCashboxes.call(localThis)).toEqual(cashboxes);
  });

  it('test computed filterCashboxes', () => {
    const localThis = {
      shop: '4',
      cashboxes,
    };
    const expectedCashboxes = JSON.parse('[{"id":5,"balance":null,"name":"Касса","created_at":"2019-09-13T15:30:11.958000+03:00","shop":{"id":4,"name":"Колбасный Рай","address":{"city":"Каизляр","region":"Дагестан","street":"улица Колбас","country":"Россия"},"phone":"+71234567890","created_by":2},"created_by":2}]');

    expect(ActionBar.computed.filterCashboxes.call(localThis)).toEqual(expectedCashboxes);
  });

  it('test computed getCashboxesIds', () => {
    const localThis = {
      filterCashboxes: cashboxes
    };
    expect(ActionBar.computed.getCashboxesIds.call(localThis)).toEqual('5,4,3,2,');
  });

  it('change items in selectCashbox',  () => {
    const selects = wrapper.findAll('.select');
    const selectShop = selects.at(0);
    const selectCashbox = selects.at(1);
    selectShop.findAll('option').at(1).setSelected();
    selectShop.trigger('input');

    expect(selectCashbox.findAll('option').at(1).text()).toMatch('Касса');
    expect(selectCashbox.findAll('option')).toHaveLength(2);
  })

});
