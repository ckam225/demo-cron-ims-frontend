import SettingsInfos from "@/components/settings/InfosCompany";
import {mount} from "@vue/test-utils";

const data = JSON.parse('[{"id":2,"name":"Братья Колбасницкие","inn":"1181111110","address":{"city":"Махачкала","region":"Дагестан","street":"улица Сервелатная","country":"Россия"},"is_available":true,"balance":"0.00","created_at":"2019-09-13T14:58:40.486000+03:00","activated_at":null,"latest_barcode":"2000000000145","tariff":null}]');

describe('', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(SettingsInfos, {
      propsData: {
        extras: data
      }
    });
  });

  it('correct render', () => {
    expect(wrapper.find('.caption + .value').text()).toMatch('Братья Колбасницкие');
  });

  it('test method editCompany', () => {
    const events = {};
    const $emit = (event, ...args) => { events[event] = [...args] };

    SettingsInfos.methods.editCompany.call({ $emit});
    expect(events['open-edit']).toEqual([true]);
  })
});
