import {createLocalVue, mount} from "@vue/test-utils";
import ActionBar from "@/components/cashboxes/ActionBar"
import ActionButton from "@/components/elements/ActionButton"
import VeeValidate from "vee-validate";

const localVue = createLocalVue();
localVue.use(VeeValidate, {
  inject: false
});

describe('test ActionBar', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(ActionBar, {
      propsData: {
        listShops: [{id: 1, shop: 'Магазин 1'}, {id: 2, shop: 'Магазин 2'}]
      },
      mocks: {
        $modal: {
          show: jest.fn()
        }
      },
      stubs: {
        NewCashbox: '<div></div>'
      }
    });
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should be class `.action-bar` ', () => {
    expect(wrapper.contains('.action-bar')).toBeTruthy();
  });

  it('vm.search should be `Магазин 1` ', () => {
    wrapper.find('.search-input').setValue('Магазин 1');
    expect(wrapper.vm.search).toBe('Магазин 1');
  });

  it('emitted event `search-changed` ` ', () => {
    wrapper.find('.search-input').setValue('Магазин 2');
    expect(wrapper.emitted('search-changed')).toBeTruthy();
  });

  it('called $modal.show', () => {
    wrapper.find(ActionButton).trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalled();
  });

  it("test computed method searchChar", () => {
    const events = {};
    const $emit = (event, ...args) => { events[event] = [...args] };

    ActionBar.methods.searchChar.call({ $emit, search: 'Магазин' });
    expect(events['search-changed']).toEqual(['Магазин']);
  });

  it("test computed method filteredShop", () => {
    const events = {};
    const $emit = (event, ...args) => { events[event] = [...args] };

    ActionBar.methods.filteredShop.call({ $emit, shop: 'Крон' });
    expect(events['shop-changed']).toEqual(['Крон']);
  });

});
