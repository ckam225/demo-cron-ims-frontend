import {mount, RouterLinkStub} from "@vue/test-utils";
import IndexTable from "@/components/cashboxes/Table";

const cashBoxes = JSON.parse('[{"id":13,"balance":null,"name":"Касса 0","created_at":"2019-09-30T15:04:30.870281+03:00","shop":{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2},"created_by":3},{"id":3,"balance":"3375.00","name":"Касса 3","created_at":"2019-09-25T18:01:43.622476+03:00","shop":{"id":2,"name":"Магазин 2","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Шамиля ","country":"Россия"},"phone":"8  555 400 40 40","created_by":2},"created_by":3},{"id":2,"balance":null,"name":"Касса 2","created_at":"2019-09-11T12:50:27.011356+03:00","shop":{"id":2,"name":"Магазин 2","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Шамиля ","country":"Россия"},"phone":"8  555 400 40 40","created_by":2},"created_by":2},{"id":1,"balance":"172.00","name":"Касса 1","created_at":"2019-09-11T12:50:20.198954+03:00","shop":{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2},"created_by":2}]');

describe('test Table', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(IndexTable, {
      propsData: {
        dataSource: cashBoxes
      },
      mocks: {
        $modal: {
          show: jest.fn()
        },
        $format: {
          balance() {
            return ''
          }
        },
        $date: {
          toDateTime(){
            return ''
          }
        }
      },
      stubs: {
        InfoCashbox: '<div></div>',
        DeleteCashbox: '<div></div>',
        RouterLink: RouterLinkStub
      }
    });
  });

  it('should be 4 tr in tbody', () => {
    expect(wrapper.element.querySelectorAll('tbody > tr')).toHaveLength(4);
  });

  it('correct render table', () => {
    expect(wrapper.text()).toMatch('Магазин 1 Касса 0');
    expect(wrapper.text()).toMatch('Магазин 2 Касса 3');
    expect(wrapper.text()).toMatch('Магазин 2 Касса 2');
    expect(wrapper.text()).toMatch('Магазин 1 Касса 1');
  });

  it('not should be class `.not-found` ', () => {
    expect(wrapper.contains('.not-found')).toBeFalsy();
  });

  it('should be class `.not-found`', () => {
    wrapper.setProps({ dataSource: [] });
    expect(wrapper.contains('.not-found')).toBeTruthy();
  });

  it('called $modal.show', () => {
    wrapper.find('.btn--delete').trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalled();
  });

  it('called $modal.show', () => {
    wrapper.find('.link-button').trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalled();
  });

});
