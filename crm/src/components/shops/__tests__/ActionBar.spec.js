import {mount} from "@vue/test-utils";
import ActionBar from "@/components/shops/ActionBar";
import ActionButton from "@/components/elements/ActionButton";

describe('test IndexTable', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(ActionBar, {
      mocks: {
        $modal: {
          show: jest.fn()
        }
      }
    });
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should be class `.action-bar` ', () => {
    expect(wrapper.contains('.action-bar')).toBeTruthy();
  });

  it('vm.search should be `Магазин 1` ', () => {
    wrapper.find('.search-input').setValue('Магазин 1');
    expect(wrapper.vm.search).toBe('Магазин 1');
  });

  it('emitted event `on-search` with value `Магазин 1` ', () => {
    wrapper.find('.search-input').setValue('Магазин 1');
    expect(wrapper.emitted('search-changed')[0]).toEqual(['Магазин 1']);
  });

  it('called $modal.show', () => {
    wrapper.find(ActionButton).trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalled();
  });

});
