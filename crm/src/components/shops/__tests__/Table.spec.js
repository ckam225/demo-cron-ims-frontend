import {mount, RouterLinkStub} from "@vue/test-utils";
import Table from  "@/components/shops/Table";

const items = JSON.parse('[{"id":2,"name":"Магазин 2","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Шамиля ","country":"Россия"},"phone":"8  555 400 40 40","created_by":2,"allStats":{"amount_for_period":"9330.00","count_positions_for_period":68},"dailyStats":{"amount_for_period":"3545.00","count_positions_for_period":12}},{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2,"allStats":{"amount_for_period":"1959.00","count_positions_for_period":35},"dailyStats":{"amount_for_period":"0.00","count_positions_for_period":0}}]');

describe('test IndexTable', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(Table, {
      propsData: {
        items,
        loading: false
      },
      stubs: {
        ProgressBar: '<div id="progress-bar"></div>',
        RouterLink: RouterLinkStub,
      }
    });
  });

  it('should be 2 tr in tbody', () => {
    expect(wrapper.element.querySelectorAll('tbody > tr')).toHaveLength(2);
  });

  it('correct render table', () => {
    expect(wrapper.text()).toMatch('Магазин 1');
    expect(wrapper.text()).toMatch('Магазин 2');
  });

  it('not should be class `not-found` ', () => {
    expect(wrapper.contains('.not-found')).toBeFalsy();
  });

  it('not render ProgressBar', () => {
    expect(wrapper.contains('#progress-bar')).toBeFalsy();
  });

  it('render ProgressBar', () => {
    wrapper.setProps({ loading: true });
    expect(wrapper.contains('#progress-bar')).toBeTruthy();
  });

  it('should be class `not-found` ', () => {
    wrapper.setProps({ items: [] });
    expect(wrapper.contains('.not-found')).toBeTruthy();
  })

});
