import Switcher from '@/components/elements/Switcher';
import {shallowMount} from '@vue/test-utils';


describe('test click switcher', () => {

  const items = JSON.parse('[{"id":0,"title":"Физ. лицо"},{"id":1,"title":"Юр. лицо"}]');
  const wrapper = shallowMount(Switcher, {
    propsData: {items}
  });

  it('click first item is selected', () => {
    expect(wrapper.vm.selected).toBe(items[0]);
  });

  it('click second item is selected', () => {
    const secondItem = wrapper.element.querySelector('.switch_item:nth-child(2)');
    secondItem.dispatchEvent(new Event('click'));
    expect(wrapper.vm.selected).toBe(items[1]);
  });

  it('emitted `on-changed` on second item with this value', () => {
    const secondItem = wrapper.element.querySelector('.switch_item:nth-child(2)');
    secondItem.dispatchEvent(new Event('click'));
    expect(wrapper.emitted('on-changed')[1]).toEqual([items[1]])
  });

  it('second item have class `active` ', () => {
    const secondItem = wrapper.element.querySelector('.switch_item:nth-child(2)');
    secondItem.dispatchEvent(new Event('click'));
    expect(secondItem.classList.contains('active')).toBeTruthy();
  })

});
