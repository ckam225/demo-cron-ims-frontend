import InputField from '@/components/elements/InputField';
import {shallowMount} from '@vue/test-utils';


describe('InputField', () => {

  const wrapper = shallowMount(InputField, {
    propsData: {
      placeholder: 'Текстовое поле',
      name: 'field'
    }
  });

  it('not input', () => {
    expect(wrapper.emitted('onInput')).toBeFalsy();
  });

  it('on input', () => {
    wrapper.vm.$emit('onInput');
    expect(wrapper.emitted('onInput')).toBeTruthy();
  });

  it('on input with value 123', () => {
    wrapper.vm.$emit('onInput', 123);
    expect(wrapper.emitted('onInput')[1]).toEqual([123])
  });

  it('set type `password` ', () => {
    wrapper.setProps({type: 'password'});
    const input = wrapper.element.querySelector('.input');
    expect(input.type).toBe('password');
  });

  it('set attr disabled', () => {
    wrapper.setProps({disabled: true});
    const input = wrapper.element.querySelector('.input');
    expect(input.disabled).toBeTruthy();
  });

  it('set attr hidden', () => {
    wrapper.setProps({hidden: true});
    const input = wrapper.element.querySelector('.input');
    expect(input.hidden).toBeTruthy();
  });

  it('set classes `field` ', () => {
    wrapper.setProps({classes: 'field'});
    const input = wrapper.element.querySelector('.input');
    expect(input.classList.contains('field')).toBeTruthy();
  });

  it('set min `1` ', () => {
    wrapper.setProps({min: 1});
    const input = wrapper.element.querySelector('.input');
    expect(input.min).toBe('1');
  });

  it('set max `20` ', () => {
    wrapper.setProps({max: 20});
    const input = wrapper.element.querySelector('.input');
    expect(input.max).toBe('20');
  });

  it('set step `2` ', () => {
    wrapper.setProps({step: 2});
    const input = wrapper.element.querySelector('.input');
    expect(input.step).toBe('2');
  });

});
