import TextField from '@/components/elements/TextField';
import {shallowMount} from '@vue/test-utils';


describe('TextField', () => {

  const wrapper = shallowMount(TextField, {
    propsData: {
      placeholder: 'Текстовое поле',
      name: 'textarea'
    }
  });

  it('not input', () => {
    expect(wrapper.emitted('onInput')).toBeFalsy();
  });

  it('on input', () => {
    wrapper.vm.$emit('onInput');
    expect(wrapper.emitted('onInput')).toBeTruthy();
  });

  it('on input with value 123', () => {
    wrapper.vm.$emit('onInput', 123);
    expect(wrapper.emitted('onInput')[1]).toEqual([123])
  })

});
