import ActionButton from '@/components/elements/ActionButton';
import {shallowMount} from '@vue/test-utils';


describe('test emitted event', () => {

  it('clicked', () => {
    const wrapper = shallowMount(ActionButton);
    wrapper.setProps({type: ''});
    wrapper.trigger('click');
    expect(wrapper.emitted('click')).toBeTruthy();
  });

  it('submitted', () => {
    const wrapper = shallowMount(ActionButton);
    wrapper.setProps({type: 'submit'});
    wrapper.trigger('click');
    expect(wrapper.emitted('submit')).toBeTruthy();
  });

  it('reset', () => {
    const wrapper = shallowMount(ActionButton);
    wrapper.setProps({type: 'reset'});
    wrapper.trigger('click');
    expect(wrapper.emitted('reset')).toBeTruthy();
  });

  it('not clicked', () => {
    const wrapper = shallowMount(ActionButton);
    wrapper.setProps({type: 'submit'});
    wrapper.trigger('click');
    expect(wrapper.emitted('click')).toBeFalsy();
  })

});


describe('test exists classes', () => {

  it('exist class `btn-fill-white` ', () => {
    const wrapper = shallowMount(ActionButton);
    expect(wrapper.classes('btn-fill-white')).toBeTruthy();
  });

  it('no exist class `btn-fill-blue` ', () => {
    const wrapper = shallowMount(ActionButton);
    expect(wrapper.classes('btn-fill-blue')).toBeFalsy();
  });


  it('not exist class `btn-fill-white` ', () => {
    const wrapper = shallowMount(ActionButton);
    wrapper.setProps({color: 'blue'});
    expect(wrapper.classes('btn-fill-white')).toBeFalsy();
  });

  it('exist class `btn-fill-blue` ', () => {
    const wrapper = shallowMount(ActionButton);
    wrapper.setProps({color: 'blue'});
    expect(wrapper.classes('btn-fill-blue')).toBeTruthy();
  })

});
