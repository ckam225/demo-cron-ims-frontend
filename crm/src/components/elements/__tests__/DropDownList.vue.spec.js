import DropDownList from '@/components/elements/DropDownList';
import {shallowMount} from '@vue/test-utils';


describe('DropDownList', () => {

  const items = [{id: 1, name: 'today'}, {id: 2, name: 'week'}, {id: 3, name: 'year'}];
  const wrapper = shallowMount(DropDownList, {
    propsData: {
      items
    }
  });

  it('ul not rendered ', () => {
    expect(wrapper.contains('ul')).toBeFalsy();
  });

  it('first element selected', () => {
    expect(wrapper.vm.selectedItem).toBe(items[0]);
  });

  it('ul rendered ', () => {
    const links = wrapper.element.querySelector('.links');
    links.dispatchEvent(new Event('click'));
    expect(wrapper.contains('ul')).toBeTruthy();
  });

  it('click second li', () => {
    const secondLi = wrapper.element.querySelector('li:nth-child(2)');
    secondLi.dispatchEvent(new Event('click'));
    expect(wrapper.vm.selectedItem).toBe(items[1]);
  });

  it('not visible', () => {
    expect(wrapper.vm.visible).toBeFalsy();
  });

});


describe('event DropDownList', () => {

  const items = [{id: 1, name: 'today'}, {id: 2, name: 'week'}, {id: 3, name: 'year'}];
  const wrapper = shallowMount(DropDownList, {
    propsData: {
      items
    }
  });

  it('event emit', () => {
    const links = wrapper.element.querySelector('.links');
    links.dispatchEvent(new Event('click'));

    const secondLi = wrapper.element.querySelector('li:nth-child(2)');
    secondLi.dispatchEvent(new Event('click'));
    expect(wrapper.emitted('dropdown-item-change')).toEqual([[items[1]]]);
  })

});
