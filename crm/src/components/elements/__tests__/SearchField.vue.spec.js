import SearchField from '@/components/elements/SearchField';
import {shallowMount} from '@vue/test-utils';


describe('test SearchField', () => {

  const wrapper = shallowMount(SearchField, {
    propsData: {
      placeholder: 'Поиск по сайту',
    }
  });

  it('no emitted onInput', () => {
    expect(wrapper.emitted('onInput')).toBeFalsy();
  });

  it('emitted onInput', () => {
    wrapper.vm.$emit('onInput');
    expect(wrapper.emitted('onInput')).toBeTruthy();
  });

  it('emitted onInput with value `shop` ', () => {
    wrapper.vm.$emit('onInput', 'shop');
    expect(wrapper.emitted('onInput')[1]).toEqual(['shop']);
  })

});
