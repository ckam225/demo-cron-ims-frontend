import Select from '@/components/elements/Select';
import {shallowMount} from '@vue/test-utils';


describe('Select', () => {

  const items = [{id: 1, name: 'item1'}, {id: 2, name: 'item2'}];
  const wrapper = shallowMount(Select, {
    propsData: {
      items
    }
  });

  it('default caption `Все` ', () => {
    expect(wrapper.vm.caption).toBe('Все');
  });

  it('no event', () => {
    expect(wrapper.emitted('input')).toBeFalsy();
  });

  it('select item 1 and event input', () => {
    const select = wrapper.element;
    select.options[1].selected = true;
    select.dispatchEvent(new Event('input'));
    expect(wrapper.emitted('input')).toEqual([['1']])
  });

  it('first options set disabled', () => {
    wrapper.setProps({disabled: true});
    const select = wrapper.element;
    expect(select.options[0].disabled).toBeTruthy();
  });

});
