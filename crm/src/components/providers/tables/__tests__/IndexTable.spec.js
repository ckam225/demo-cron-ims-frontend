import IndexTable from '@/components/providers/tables/IndexTable';
import {createLocalVue, mount} from '@vue/test-utils';
import VModal from 'vue-js-modal';

const providers = JSON.parse('[{"id":3,"name":"Атлон","email":"atlon@gmail.com","phone":"8 700 500 80 70","address":"","created_by":3},{"id":2,"name":"Сотовик","email":"sotovik@mail.ru","phone":"8 900 700 60 30","address":"","created_by":2},{"id":1,"name":"Импульс","email":"impulse@mail.ru","phone":"8 800 500 80 70","address":"","created_by":2}]');
const json = '{"country":"Russia","region":"Daghestan","city":"Makhachkala","street":"Irchi Kazaka"}';

const localVue = createLocalVue();
localVue.use(VModal);


describe('test IndexTable', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = mount(IndexTable, {
      localVue,
      propsData: {
        providers
      },
      mocks: {
        $modal: {
          show: jest.fn()
        }
      }
    });
  });

  it('should be 3 tr in tbody', () => {
    expect(wrapper.element.querySelectorAll('tbody > tr')).toHaveLength(3);
  });

  it('correct render table', () => {
    expect(wrapper.text()).toMatch('Атлон');
    expect(wrapper.text()).toMatch('Импульс');
    expect(wrapper.text()).toMatch('Сотовик');
  });

  it('correct parse address', () => {
    const result = wrapper.vm.parseAddress(json);
    expect(result).toBe('Russia, Daghestan, Makhachkala, Irchi Kazaka');
  });

  it('called $modal.show', () => {
    wrapper.find('.btn--delete').trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalled();
  });

});
