import {mount} from "@vue/test-utils";
import IndexTable from "@/components/clients/tables/IndexTable";

const clients = JSON.parse('[{"id":1,"card":null,"discount":0,"name":"Розничный клиент","email":"","phone":"","gender":"","is_individual":true,"requisites":{},"is_retail":true,"created_at":"2019-09-11T12:48:18.648765+03:00","created_by":2},{"id":2,"card":{"id":1,"card_number":"0002","created_at":"2019-09-11T12:59:04.354172+03:00","client":2,"created_by":2},"discount":0,"name":"Mobile shop","email":"mob_shop@mail.ru","phone":"8 800 800 10 40","gender":"","is_individual":false,"requisites":{"inn":"0003","kpp":"0005","ogrn":"0004"},"is_retail":false,"created_at":"2019-09-11T12:59:04.305845+03:00","created_by":2},{"id":9,"card":{"id":4,"card_number":"454545454545","created_at":"2019-09-18T18:58:24.752604+03:00","client":9,"created_by":8},"discount":0,"name":"John Smith","email":"manager@mail.ru","phone":"8 800 600 30 40","gender":"MALE","is_individual":true,"requisites":{},"is_retail":true,"created_at":"2019-09-18T18:58:24.712388+03:00","created_by":8}]');

describe('test IndexTable', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(IndexTable, {
      propsData: {
        clients
      },
      mocks: {
        $modal: {
          show: jest.fn()
        }
      }
    });
  });

  it('should be 3 tr in tbody', () => {
    expect(wrapper.element.querySelectorAll('tbody > tr').length).toBe(3);
  });

  it('correct render table', () => {
    expect(wrapper.text()).toMatch('Розничный клиент');
    expect(wrapper.text()).toMatch('Mobile shop');
    expect(wrapper.text()).toMatch('John Smith');
  });

  it('should be `Розничный` ', () => {
    expect(wrapper.vm.is_retail(true)).toBe('Розничный');
  });

  it('should be `Оптовый` ', () => {
    expect(wrapper.vm.is_retail(false)).toBe('Оптовый');
  });

  it('should be `Оптовый` ', () => {
    expect(wrapper.vm.is_retail()).toBe('Оптовый');
  });

  it('called $modal.show', () => {
    wrapper.find('.btn--delete').trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalled();
  });

});
