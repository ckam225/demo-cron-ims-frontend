import {mount} from "@vue/test-utils";
import IndexActionBar from "@/components/clients/actionbars/IndexActionBar";
import ActionButton from "@/components/elements/ActionButton";

describe('test IndexTable', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(IndexActionBar, {
      mocks: {
        $modal: {
          show: jest.fn()
        }
      }
    });
  });

  it('should be a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should be class `.action-bar` ', () => {
    expect(wrapper.contains('.action-bar')).toBeTruthy();
  });

  it('vm.search should be `Mobile shop` ', () => {
    wrapper.find('.search-input').setValue('Mobile shop');
    expect(wrapper.vm.search).toBe('Mobile shop');
  });

  it('emitted event `on-search` with value `Mobile shop` ', () => {
    wrapper.find('.search-input').setValue('Mobile shop');
    expect(wrapper.emitted('on-search')[0]).toEqual(['Mobile shop']);
  });

  it('called $modal.show', () => {
    wrapper.find(ActionButton).trigger('click');
    expect(wrapper.vm.$modal.show).toHaveBeenCalled();
  });

});
