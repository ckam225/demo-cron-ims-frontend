import {mount, RouterLinkStub} from '@vue/test-utils';
import ActionBar from '@/components/procurements/actionbars/IndexActionBar';

const shops = JSON.parse('[{"id":8,"name":"Магазин 2","address":{"city":"Махачкала ","region":"Дагестан","street":"Шамиля","country":"Россия"},"phone":"8 800 400 40 40","created_by":16},{"id":7,"name":"Магазин 1","address":{"city":"Махачкала ","region":"Дагестан","street":"Ирчи Казака 11 а","country":"Россия"},"phone":"8 800 500 50 50","created_by":16}]');

describe('', () => {
  let wrapper;
  beforeEach( () => {
    wrapper = mount(ActionBar, {
      propsData: {
        placeholder: 'Поиск по номеру договора',
        buttonText: 'Новое поступление',
        componentUrl: 'procurements/new',
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
          commit: jest.fn(),
          getters: {
            shops
          }
        }
      },
      stubs: {
        RouterLink: RouterLinkStub
      }
    });
  });

  it('has class `.action-bar` ', () => {
    expect(wrapper.contains('.action-bar')).toBeTruthy();
  });

  it('test method handlerForSelectShop', () => {
    const localThis = {
      $store: {
        dispatch: jest.fn(),
        commit: jest.fn()
      },
    };
    ActionBar.methods.handlerForSelectShop.call(localThis, 5);
    expect(localThis.$store.commit).toHaveBeenCalledWith('SET_SHOPID', 5);
    expect(localThis.$store.dispatch).toHaveBeenCalledWith('fetchProcurements');
  });

  it('test method handlerForDateSort', () => {
    const localThis = {
      $store: {
        dispatch: jest.fn()
      },
    };
    ActionBar.methods.handlerForDateSort.call(localThis, 'dates');
    expect(localThis.$store.dispatch).toHaveBeenNthCalledWith(1, 'setArrayDate', 'dates');
    expect(localThis.$store.dispatch).toHaveBeenNthCalledWith(2, 'fetchProcurements');
  });


});
