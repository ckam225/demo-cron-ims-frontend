import '@/api'; // error otherwise
import InputField from '@/components/elements/InputField';
import NewActionBar from '@/components/procurements/actionbars/NewSingleActionBar';
import ProcurementStore from '@/store/modules/procurement.js';
import {createLocalVue, RouterLinkStub, shallowMount} from '@vue/test-utils';
import Vuex from 'vuex';

const {mutations} = ProcurementStore;

const localVue = createLocalVue();
localVue.use(Vuex);

const positions = [{purchases: 100, wholesale: null, retail: null}];
const roundingAccuracy = 0.05;

const store = new Vuex.Store({
  getters: {
    newPositions() { return positions; },
    shops() { return []; },
    providers() { return []; },
    roundingAccuracy() { return 1; },
  },
  actions: {
    getListShops() { return []; },
    fetchProviders() {},
  },
  mutations,
  state: {
    newPositions: positions,
    roundingAccuracy,
  }
});


describe('NewSingleActionBar', () => {

  const newActionBarWrapper = shallowMount(NewActionBar, {
    store,
    localVue,
    stubs: {
      RouterLink: RouterLinkStub,
      InputField,
    },
  });

  it('should be a Vue instance', () => {
    expect(newActionBarWrapper.isVueInstance()).toBeTruthy();
  });

  it('should round wholesale', () => {
    const wholesaleMarginInput = newActionBarWrapper.find('.test-wholesaleMargin');

    wholesaleMarginInput.setValue(45.123);
    expect(store.getters.newPositions[0].wholesale).toBe(145.1);

    wholesaleMarginInput.setValue(45.129);
    expect(store.getters.newPositions[0].wholesale).toBe(145.15);
  });

  it('should round retail', () => {
    const retailMarginInput = newActionBarWrapper.find('.test-retailMargin');

    retailMarginInput.setValue(1.26);
    expect(store.getters.newPositions[0].retail).toBe(101.25);

    retailMarginInput.setValue(2.19);
    expect(store.getters.newPositions[0].retail).toBe(102.2);
  });

});
