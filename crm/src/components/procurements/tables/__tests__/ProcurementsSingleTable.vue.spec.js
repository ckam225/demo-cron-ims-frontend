import ProcurementsSingleTable from '@/components/procurements/tables/SingleTable';
import {mount, shallowMount} from '@vue/test-utils';


describe('totals in SingleProcurement', () => {

  const $route = {
    name: 'ProcurementEditSingle'
  };

  let data = `{"id":24,"shop":{"id":2,"name":"Магазин 2","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Шамиля ","country":"Россия"},"phone":"8  555 400 40 40","created_by":2},"provider":{"id":2,"name":"Сотовик","email":"sotovik@mail.ru","phone":"8 900 700 60 30","address":"","created_by":2},"created_by":{"id":3,"email":"manager@mail.ru"},"positions":[{"id":92,"product":{"id":125,"photos":[],"price":{"product":125,"purchases":"55.00","wholesale":"65.00","retail":"75.00","discount_percent":0,"created_at":"2019-09-19T17:35:33.751951+03:00","updated_at":"2019-09-19T17:37:17.056315+03:00"},"name":"iPad Air","product_type":1,"description":"","barcode":"","category":15,"created_by":3},"count":20,"balance":19,"purchases":"55.00","wholesale":"65.00","retail":"75.00","created_at":"2019-09-19T17:37:17.118710+03:00"},{"id":90,"product":{"id":126,"photos":[],"price":{"product":126,"purchases":"50.00","wholesale":"60.00","retail":"70.00","discount_percent":0,"created_at":"2019-09-19T17:35:42.481554+03:00","updated_at":"2019-09-19T17:37:17.000119+03:00"},"name":"iPad","product_type":1,"description":"","barcode":"","category":15,"created_by":3},"count":20,"balance":20,"purchases":"50.00","wholesale":"60.00","retail":"70.00","created_at":"2019-09-19T17:37:17.118494+03:00"},{"id":91,"product":{"id":127,"photos":[],"price":{"product":127,"purchases":"45.00","wholesale":"55.00","retail":"65.00","discount_percent":0,"created_at":"2019-09-19T17:36:25.940586+03:00","updated_at":"2019-09-19T17:37:17.026181+03:00"},"name":"iPad mini","product_type":1,"description":"","barcode":"","category":15,"created_by":3},"count":20,"balance":20,"purchases":"45.00","wholesale":"55.00","retail":"65.00","created_at":"2019-09-19T17:37:17.118572+03:00"},{"id":93,"product":{"id":124,"photos":[],"price":{"product":124,"purchases":"70.00","wholesale":"80.00","retail":"90.00","discount_percent":0,"created_at":"2019-09-19T17:35:20.140102+03:00","updated_at":"2019-09-19T17:37:17.088056+03:00"},"name":"iPad Pro","product_type":1,"description":"","barcode":"","category":15,"created_by":3},"count":20,"balance":20,"purchases":"70.00","wholesale":"80.00","retail":"90.00","created_at":"2019-09-19T17:37:17.118775+03:00"}],"number":22,"procurement_type":1,"is_posted":true,"created_at":"2019-09-19T17:37:16.964983+03:00","posted_at":"2019-09-19T17:37:25.779412+03:00","move":null}`;
  data = JSON.parse(data);

  const wrapper = shallowMount(ProcurementsSingleTable, {
    propsData: {
      procurement: data,
      newPositions: [],
      isRemove: false,
      isEdit: false,
      isNewProcurement: false,
      isNotPosted: false
    },
    mocks: {
      $route
    }
  });

  it('is exist total count', () => {
    expect(wrapper.contains('#test-total-count')).toBeTruthy();
  });

  it('total count value 80', () => {
    expect(wrapper.vm.totalCount).toBe(80);
  });

  it('render value 80 in elem with #test-total-count', () => {
    const HTMLElement = wrapper.element;
    const elem = HTMLElement.querySelector('#test-total-count');
    expect(parseInt(elem.textContent)).toBe(80);
  });

  it('is exist total purchases', () => {
    expect(wrapper.contains('#test-total-purchases')).toBeTruthy();
  });

  it('total purchases value 4400', () => {
    expect(wrapper.vm.totalPurchases).toBe(4400);
  });

  it('render table row', () => {
    const HTMLElement = wrapper.element;
    const tableRows = HTMLElement.querySelector('.procurement-table__body');
    expect(tableRows.children.length).toBe(4);
  })

});


describe('render positions in SingleProcurement', () => {

  const $route = {
    name: 'ProcurementNewSingle'
  };

  let data = `[{"product":34,"name":"AMD Ryzen™ 3","count":0,"retail":0,"wholesale":0,"purchases":0},{"product":36,"name":"AMD Ryzen™ 5","count":0,"retail":0,"wholesale":0,"purchases":0},{"product":35,"name":"AMD Ryzen™ 7","count":0,"retail":0,"wholesale":0,"purchases":0}]`;
  data = JSON.parse(data);

  const wrapper = mount(ProcurementsSingleTable, {
    propsData: {
      procurement: {},
      newPositions: data,
      isRemove: false,
      isEdit: false,
      isNewProcurement: true,
      isNotPosted: true
    },
    mocks: {
      $route
    }
  });

  it('AMD Ryzen™ 3 render in ProcurementsSingleTable', () => {
    expect(wrapper.html()).toMatch('AMD Ryzen™ 3');
  });

  it('AMD Ryzen™ 5 render in ProcurementsSingleTable', () => {
    expect(wrapper.html()).toMatch('AMD Ryzen™ 5');
  });

  it('AMD Ryzen™ 7 render in ProcurementsSingleTable', () => {
    expect(wrapper.html()).toMatch('AMD Ryzen™ 7');
  })

});
