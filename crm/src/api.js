import vm from '@/main';
import store from '@/store';
import axios from 'axios';
import storage from '@/plugins/storage';

const api = axios.create({
  baseURL: process.env.VUE_APP_API_URL
});


api.interceptors.request.use(
  config => {
    if (storage.existsAuthUser()) {
      const user = storage.getAuthUser();
      config.headers.common['Authorization'] = `Token ${user.token}`;
    }
    return config;
  },
  Promise.reject
);


api.interceptors.response.use(
  response => response,
  error => {
    if (error.response.status === 401) {
      store.commit('SET_AUTH_OUT');
    }
    return Promise.reject(error);
  }
);


api.interceptors.response.use(
  response => response,
  error => {
    if (error.response.status === 400) {
      if (error.response.data) {
        vm.$validator.errors.clear();

        for (let key in error.response.data) {
          vm.$validator.errors.add({field: key, msg: error.response.data[key][0]})
        }
      }
    }

    return Promise.reject(error);
  }
);


export default api;
