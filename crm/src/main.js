import Chart from 'chart.js'
import VeeValidate, {Validator} from 'vee-validate';
import Vue from 'vue';
import Chartkick from 'vue-chartkick'
import VModal from 'vue-js-modal';
import VueTreeNavigation from 'vue-tree-navigation';
import VueNoty from 'vuejs-noty';

import App from './App';
import blankLayout from './layouts/Blank';
import defaultLayout from './layouts/Default';
import scrollWrap from './layouts/ScrollWrap';
import Utils from './plugins/utils';
import router from './router';
import store from './store';

Vue.component('default-layout', defaultLayout);
Vue.component('blank-layout', blankLayout);
Vue.component('scroll-wrap', scrollWrap);

Vue.config.productionTip = false;
Vue.prototype.$eventBus = new Vue();
Vue.use(VueTreeNavigation);
Vue.use(VeeValidate, {inject: false});
Validator.localize('ru', require('vee-validate/dist/locale/ru'));
Vue.use(VModal);
Vue.use(VueNoty);
Vue.use(Utils);
Vue.use(Chartkick.use(Chart));

export default new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app');
