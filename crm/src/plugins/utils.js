/* eslint-disable */
export default {
  install(Vue) {

    const vm = new Vue()

    Vue.prototype.$months_of_years = [
      { id: 1, name: "январь" },
      { id: 2, name: "февраль" },
      { id: 3, name: "март" },
      { id: 4, name: "апрель" },
      { id: 5, name: "май" },
      { id: 6, name: "июнь" },
      { id: 7, name: "июль" },
      { id: 8, name: "август" },
      { id: 9, name: "сентябрь" },
      { id: 10, name: "октябрь" },
      { id: 11, name: "ноябрь" },
      { id: 12, name: "декабрь" },
    ];
    Vue.prototype.$days_of_week = [
      { id: 1, name: "Понедельник" },
      { id: 2, name: "Вторник" },
      { id: 3, name: "Среда" },
      { id: 4, name: "Четверг" },
      { id: 5, name: "Пятница" },
      { id: 6, name: "Суббота" },
      { id: 7, name: "Воскресенье" }
    ];
    Vue.prototype.$stats_periods = [
      { id: 1, name: "сегодня" },
      { id: 2, name: "неделю" },
      { id: 3, name: "год" },
    ],
      Vue.prototype.$stats = {
        averagePercent: function (initial, balance) {
          if (initial === 0)
            return 0
          let ecart = (balance - initial) / initial
          return ecart * 100;
        }
      }
    Vue.prototype.$format = {
      decimal: function (num) {
        return parseFloat(num).toFixed(2)
      },
      toAddress: function (address) {
        const addr = address !== null ? Object.values(address).join(', ') : []
        const items = []
        for (var i; i < addr.length; i++)
          if (addr[i] !== "")
            items.push(addr[i])
        return items.length > 0 ? items.join(', ') : ''
      },
      balance: function (e) {
        return e || '0.0';
      },
      number: function (e) {
        if (e > 0 && e < 10) return "0" + e;
        return e;
      }
    }

    Vue.prototype.$date = {

      getDuration: function (dateStart, dateEnd = new Date()) {
        const date1 = new Date(dateStart)
        const date2 = new Date(dateEnd)
        const ecart = Math.abs(date2.getTime() - date1.getTime())
        var secs = Math.round(ecart / 1000)
        if (secs > 59) {
          var mins = (secs - (secs % 60)) / 60
          if (mins > 60) {
            var hours = (mins - (mins % 60)) / 60
            mins = mins % 60
            if (hours > 23) {
              var days = (hours - (hours % 24)) / 24
              hours = hours % 24
              return days + " д " + hours + " ч " + mins + " мин "
            }
            return hours + " ч " + mins + " мин "
          }
          return mins + " мин "
        }
        return "только открыто"
      },
      toDateTime: function (e) {
        if(!e) return;
        const date = new Date(e);
        return date.toLocaleString('ru-RU');
      },
      toDate: function (e) {
        if (e) {
          const date = new Date(e);
          return date.toLocaleDateString('ru-RU');
        }
        return ''
      },
      toLongDateTime: function (e) {
        const day = new Date(e).getDate();
        const month = new Date(e).getMonth();
        const hour = new Date(e).getHours();
        const mins = new Date(e).getMinutes();
        return day + " " + vm.$months_of_years[month].name + " " + hour + ":" + mins
      },
      toLongDate: function (e) {
        const date = new Date(e);
        const str = [date.getDate(), vm.$months_of_years[date.getMonth()].name, date.getFullYear()]
        return str.join(" ")
      },
      toTime: function (e) {
        const date = new Date(e);
        const str = [date.getHours(), date.getMinutes()]
        return str.join(":")
      },
      toDateFormat: function (e, separator = '.') {
        const date = new Date(e);
        return date.getFullYear() + separator + (date.getMonth() + 1) + separator + date.getDate();
      },
      getCurrent() {
        const date = new Date()
        const separator = "-"
        return date.getFullYear() + separator + (date.getMonth() + 1) + separator + date.getDate()
          + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
      }

    }

    Vue.prototype.$toast = {
      success: (msg) => {
        vm.$noty.success(msg, { layout: 'bottomRight', theme: 'relax', timeout: 2000 })
      },
      error: (msg) => {
        vm.$noty.error(msg, { layout: 'bottomRight', theme: 'relax', timeout: 2000 })
      },
      errors: (error) => {
        const msg = Object.values(error)
        if (msg.length > 0) {
          msg.forEach((m) => {
            vm.$noty.error(m[0], { layout: 'bottomRight', theme: 'relax', timeout: 2000 })
          })
        }
      }
    }

    Vue.prototype.$settings = {
      refresh_data_delai: 120000, // refresh data after 2 minutes
    }

    Vue.prototype.$ResetValidator = function () {
      this.$nextTick(() => {
        this.errors.clear();
        this.$nextTick(() => {
          this.$validator.reset();
        });
      });
    }

  }
}
