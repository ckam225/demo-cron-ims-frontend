import Vue from 'vue';
import VueRouter from 'vue-router';

import access from './middleware/access'
import auth from './middleware/auth';
import guest from './middleware/guest';

import Login from './pages/auth/Login';
import Register from './pages/auth/Register';
import ResetPassword from './pages/auth/ResetPassword';
import ResetSuccess from './pages/auth/ResetSuccess';
import RestorePassword from './pages/auth/RestorePassword';

import Cashboxes from './pages/cashboxes/Index'

import Clients from './pages/clients/Index';

import EmployeeSlug from './pages/employee/_Slug'
import EmployeeEdit from './pages/employee/Edit'
import Employee from './pages/employee/Index';
import EmployeeInfos from './pages/employee/Infos'

import NotFound from './pages/errors/404';

import Home from './pages/Home';

import MovesRouter from './pages/moves/_Slug';
import MovesIndex from './pages/moves/Index';
import MovesNew from './pages/moves/New';
import MoveSingle from './pages/moves/_Single';
import MovesInfo from './pages/moves/Info';
import MovesEdit from './pages/moves/Edit';


import Positions from './pages/positions/Index'

import ProcurementSlug from './pages/procurements/_Slug'
import ProcurementsIndex from './pages/procurements/Index'
import ProcurementsSingle from './pages/procurements/Single'

import Products from './pages/products/Index';

import Providers from './pages/providers/Index';

import ReturnsRouter from './pages/returns/_Slug';
import ReturnsIndex from './pages/returns/Index';
import ReturnsInfo from './pages/returns/Info'

import SalesSlug from './pages/sales/_Slug';
import SalesIndex from './pages/sales/Index';
import SalesSingle from './pages/sales/Single';

import SettingsSlug from './pages/settings/_Slug';
import SettingsIndex from './pages/settings/Index';
import SettingsUser from './pages/settings/User';

import Shifts from './pages/shifts/Index';

import ShopsSlug from './pages/shops/_Slug'
import ShopsEdit from './pages/shops/Edit'
import Shops from './pages/shops/Index'
import ShopsInfos from './pages/shops/Infos'

import DiscountRouter from './pages/marketing/discounts/_Slug';
import DiscountIndex from './pages/marketing/discounts/Index';
import DiscountCreate from './pages/marketing/discounts/Create';

import _Loyalty from './pages/marketing/loyalties/_Index';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home,
      name: 'Home',
      meta: {
        middleware: [guest, access]
      }
    },
    {
      path: '/products',
      component: Products,
      name: 'Products',
      meta: {
        type: 1,
        middleware: [guest, access]
      }
    },
    {
      path: '/services',
      component: Products,
      name: 'Services',
      meta: {
        type: 2,
        middleware: [guest, access]
      }
    },
    {
      path: '/shifts',
      component: Shifts,
      name: 'Shifts',
      meta: {
        middleware: [guest, access]
      }
    },
    {
      path: '/positions',
      component: Positions,
      name: 'Positions',
      meta: {
        middleware: [guest, access]
      }
    },
    {
      path: '/procurements',
      component: ProcurementSlug,
      children: [
        {
          path: '',
          component: ProcurementsIndex,
          name: 'ProcurementsIndex',
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: 'new',
          component: ProcurementsSingle,
          name: 'ProcurementNewSingle',
          meta: {
            type: 1, middleware: [guest, access]
          }
        },
        {
          path: ':id',
          component: ProcurementsSingle,
          name: 'ProcurementSingle',
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: ':id/edit',
          component: ProcurementsSingle,
          name: 'ProcurementEditSingle',
          meta: {
            middleware: [guest, access]
          }
        },
      ]
    },
    {
      path: '/sales',
      component: SalesSlug,
      children: [
        {
          path: '',
          component: SalesIndex,
          name: 'SalesIndex',
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: ':id',
          component: SalesSingle,
          name: 'SalesSingle',
          meta: {
            middleware: [guest, access]
          }
        },
      ]
    },
    {
      path: '/shops',
      component: Shops,
      name: 'Shops',
      meta: {
        middleware: [guest, access]
      }
    },
    {
      path: '/shops/:id',
      component: ShopsSlug,
      children: [
        {
          path: '/',
          component: ShopsInfos,
          name: 'ShopsInfos',
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: 'edit',
          component: ShopsEdit,
          name: 'ShopsEdit',
          meta: {
            middleware: [guest, access]
          }
        },
      ]
    },
    {
      path: '/employees',
      component: Employee,
      name: 'EmployeeIndex',
      meta: {
        middleware: [guest, access]
      }
    },
    {
      path: '/employees/:id',
      component: EmployeeSlug,
      children: [
        {
          path: '/',
          component: EmployeeInfos,
          name: 'employeeInfos',
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: 'edit',
          component: EmployeeEdit,
          name: 'employeeEdit',
          meta: {
            middleware: [guest, access]
          }
        }
      ]
    },
    {
      path: '/register',
      component: Register,
      name: 'Register',
      meta: {
        layout: 'blank',
        middleware: auth
      }
    },
    {
      path: '/login',
      component: Login,
      name: 'Login',
      meta: {
        layout: 'blank',
        middleware: auth
      }
    },
    {
      path: '/password/restore',
      component: RestorePassword,
      name: 'RestorePassword',
      meta: {
        layout: 'blank',
        middleware: auth
      }
    },
    {
      path: '/password/restore/success',
      component: ResetSuccess,
      name: 'RestoreSuccess',
      meta: {
        layout: 'blank',
        middleware: auth
      }
    },
    {
      path: '/password/reset/success',
      component: ResetSuccess,
      name: 'ResetSuccess',
      meta: {
        layout: 'blank',
        middleware: auth
      }
    },
    {
      path: '/password-reset/:uid/:token',
      component: ResetPassword,
      name: 'ResetPassword',
      meta: {
        layout: 'blank',
        middleware: auth
      }
    },


    {
      path: '*',
      component: NotFound,
      name: 'Error404',
      meta: {
        layout: 'blank'
      }
    },
    {
      path: '/404',
      component: NotFound,
      name: 'NotFound',
      meta: {
        layout: 'blank'
      }
    },
    {
      path: '/cashboxes',
      component: Cashboxes,
      name: 'Cashboxes',
      meta: {
        middleware: [guest, access]
      }
    },
    {
      path: '/settings',
      component: SettingsSlug,
      children: [
        {
          path: '/',
          component: SettingsIndex,
          name: 'SettingsIndex',
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: 'user',
          component: SettingsUser,
          name: 'SettingsUser',
          meta: {
            middleware: [guest, access]
          }
        },
      ]
    },
    {
      path: '/clients',
      component: Clients,
      name: 'ClientsIndex',
      meta: {
        middleware: [guest, access]
      }
    },
    {
      path: '/providers',
      component: Providers,
      name: 'ProvidersIndex',
      meta: {
        middleware: [guest, access]
      }
    },
    {
      path: '/moves',
      component: MovesRouter,
      children: [
        {
          path: '/',
          name: 'Moves',
          component: MovesIndex,
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: 'move',
          name: 'MovesNew',
          component: MovesNew,
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: ':id',
          component: MoveSingle,
          children:[
            {
              name: 'MovesInfo',
              component: MovesInfo,
              path:'',
              meta: {
                middleware: [guest, access]
              },
            },
            {
              name: 'MovesEdit',
              component: MovesEdit,
              path:'edit',
              meta: {
                middleware: [guest, access]
              },
            }
          ]
        },
      ]
    },
    {
      path: '/returns',
      component: ReturnsRouter,
      children: [
        {
          path: '/',
          name: 'Returns',
          component: ReturnsIndex,
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: ':id',
          name: 'ReturnsInfo',
          component: ReturnsInfo,
          meta: {
            middleware: [guest, access]
          }
        }
      ]
    },
    {
      path: '/discounts',
      component: DiscountRouter,
      children: [
        {
          path: '/',
          component: DiscountIndex,
          name: 'DiscountIndex',
          meta: {
            middleware: [guest, access]
          }
        },
        {
          path: 'create',
          component: DiscountCreate,
          name: 'DiscountCreate',
          meta: {
            middleware: [guest, access]
          }
        },
      ]
    },
    {
      path: '/loyalties',
      component: _Loyalty,
    }
  ]
});

function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];
  // Если не существует промежуточного программного обеспечения,
  // возвращается обратный вызов по умолчанию `next ()`.
  if (!subsequentMiddleware) return context.next;

  return (...parameters) => {
    // Сначала запускаем обратный вызов Vue Router `next ()` по умолчанию.
    context.next(...parameters);
    // Затем запускаем следующее Middleware с новым
    // `nextMiddleware ()` обратный вызов.
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({...context, next: nextMiddleware});
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware) ? to.meta.middleware : [to.meta.middleware];
    const context = {from, next, router, to};
    const nextMiddleware = nextFactory(context, middleware, 1);
    return middleware[0]({...context, next: nextMiddleware});
  }
  return next();
});

export default router
