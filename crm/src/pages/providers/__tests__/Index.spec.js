import {createLocalVue, mount} from "@vue/test-utils";
import Index from "@/pages/providers/Index";
import SearchField from "@/components/elements/SearchField";
import ActionButton from  "@/components/elements/ActionButton"
import Vuex from "vuex";
import VModal from "vue-js-modal";
import VeeValidate from "vee-validate";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VModal);
localVue.use(VeeValidate, {
  inject: false
});

const providers = JSON.parse('[{"id":3,"name":"Атлон","email":"atlon@gmail.com","phone":"8 700 500 80 70","address":"","created_by":3},{"id":2,"name":"Сотовик","email":"sotovik@mail.ru","phone":"8 900 700 60 30","address":"","created_by":2},{"id":1,"name":"Импульс","email":"impulse@mail.ru","phone":"8 800 500 80 70","address":"","created_by":2}]');

const store = new Vuex.Store({
  getters: {
    providers() {
      return providers
    }
  },
  actions: {
    fetchProviders: jest.fn()
  }
});

describe('', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(Index, {
      localVue,
      store
    })
  });

  it('test computed property filteredProviders', () => {
    const localThis = {
      providers,
      search: 'Атлон'
    };
    const result = Index.computed.filteredProviders.call(localThis);
    expect(result).toHaveLength(1);
    expect(result).toContainEqual({id: 3,name: 'Атлон', email: 'atlon@gmail.com', phone: '8 700 500 80 70', address: '', created_by: 3 });
  });

  it('test search with value `Атлон` ', () => {
    wrapper.find('.search-input').setValue('Атлон');
    wrapper.find(SearchField).trigger('input');
    expect(wrapper.element.querySelectorAll('.table > tbody > tr')).toHaveLength(1);
    expect(wrapper.text()).toMatch('Атлон');
  });

  it('not render modal `create-provider` ', () => {
    expect(wrapper.contains('[data-modal="create-provider"]')).toBeFalsy();
  });

  it('render modal `create-provider` ', () => {
    wrapper.find(ActionButton).trigger('click');
    expect(wrapper.contains('[data-modal="create-provider"]')).toBeTruthy();
  });

});
