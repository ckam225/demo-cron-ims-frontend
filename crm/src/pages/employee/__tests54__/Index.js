import {createLocalVue, mount, RouterLinkStub} from '@vue/test-utils';
import EmployeesIndex from '@/pages/employee/Index';
import VModal from "vue-js-modal";
import VeeValidate from "vee-validate";

const localVue = createLocalVue();

localVue.use(VModal);
localVue.use(VeeValidate, {
  inject: false
});

const filteredEmployees = JSON.parse('[{"id":18,"current_work":{"id":18,"shop":8,"position":10,"is_cashier":1,"salary":90000},"user":{"id":20,"email":"director@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:29:58.323709+03:00"},"name":"Магомедов Магомед Магомедович","phone":"+7 999 999 99 99","gender":"MALE","document":{"number":"487161563","serial":"4566","issued_by":"Россия"},"birthday":"2000-10-01","created_at":"2019-10-30T16:29:58.525012+03:00","created_by":16},{"id":17,"current_work":{"id":17,"shop":8,"position":12,"is_cashier":2,"salary":50000},"user":{"id":19,"email":"cashier2@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:28:36.050926+03:00"},"name":"Сергеева Инна Вячеславовна","phone":"+7 888 800 80 80","gender":"FEMALE","document":{"number":"3154851","serial":"7974","issued_by":"Россия"},"birthday":"2000-07-09","created_at":"2019-10-30T16:28:36.258738+03:00","created_by":16},{"id":16,"current_work":{"id":16,"shop":7,"position":12,"is_cashier":2,"salary":50000},"user":{"id":18,"email":"cashier@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:27:19.200017+03:00"},"name":"Ахмедханова Фаина Исламовна","phone":"+7 777 456 78 90","gender":"FEMALE","document":{"number":"474575","serial":"7847","issued_by":"Россия"},"birthday":"2000-10-01","created_at":"2019-10-30T16:27:19.425571+03:00","created_by":16},{"id":15,"current_work":{"id":15,"shop":7,"position":11,"is_cashier":1,"salary":70000},"user":{"id":17,"email":"manager@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:25:49.999117+03:00"},"name":"Ахмедов Ислам Мухтарович","phone":"+71234567890","gender":"MALE","document":{"number":"02","serial":"01","issued_by":"Россия"},"birthday":"2000-10-01","created_at":"2019-10-30T16:25:50.269215+03:00","created_by":16},{"id":14,"current_work":{"id":14,"shop":7,"position":9,"is_cashier":1,"salary":null},"user":{"id":16,"email":"cron@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:15:53.522027+03:00"},"name":"cron@mail.ru","phone":"","gender":"MALE","document":{},"birthday":null,"created_at":"2019-10-30T16:15:53.763740+03:00","created_by":16}]');
const positions = JSON.parse('[{"id":12,"name":"Кассир","created_by":16},{"id":11,"name":"Менеджер","created_by":16},{"id":10,"name":"Директор","created_by":16},{"id":9,"name":"Управляющий","created_by":16}]');
const shops = JSON.parse('[{"id":8,"name":"Магазин 2","address":{"city":"Махачкала ","region":"Дагестан","street":"Шамиля","country":"Россия"},"phone":"8 800 400 40 40","created_by":16},{"id":7,"name":"Магазин 1","address":{"city":"Махачкала ","region":"Дагестан","street":"Ирчи Казака 11 а","country":"Россия"},"phone":"8 800 500 50 50","created_by":16}]');
const works = JSON.parse('[{"id":14,"is_cashier":1,"salary":null,"is_active":true,"created_at":"2019-10-30T16:15:53.805231+03:00","company":5,"shop":{"id":7,"name":"Магазин 1","address":{"city":"Махачкала ","region":"Дагестан","street":"Ирчи Казака 11 а","country":"Россия"},"phone":"8 800 500 50 50","created_by":16},"position":{"id":9,"name":"Управляющий","created_by":16},"employee":14,"created_by":16},{"id":15,"is_cashier":1,"salary":70000,"is_active":true,"created_at":"2019-10-30T16:25:50.290210+03:00","company":5,"shop":{"id":7,"name":"Магазин 1","address":{"city":"Махачкала ","region":"Дагестан","street":"Ирчи Казака 11 а","country":"Россия"},"phone":"8 800 500 50 50","created_by":16},"position":{"id":11,"name":"Менеджер","created_by":16},"employee":15,"created_by":16},{"id":16,"is_cashier":2,"salary":50000,"is_active":true,"created_at":"2019-10-30T16:27:19.447882+03:00","company":5,"shop":{"id":7,"name":"Магазин 1","address":{"city":"Махачкала ","region":"Дагестан","street":"Ирчи Казака 11 а","country":"Россия"},"phone":"8 800 500 50 50","created_by":16},"position":{"id":12,"name":"Кассир","created_by":16},"employee":16,"created_by":16},{"id":17,"is_cashier":2,"salary":50000,"is_active":true,"created_at":"2019-10-30T16:28:36.275358+03:00","company":5,"shop":{"id":8,"name":"Магазин 2","address":{"city":"Махачкала ","region":"Дагестан","street":"Шамиля","country":"Россия"},"phone":"8 800 400 40 40","created_by":16},"position":{"id":12,"name":"Кассир","created_by":16},"employee":17,"created_by":16},{"id":18,"is_cashier":1,"salary":90000,"is_active":true,"created_at":"2019-10-30T16:29:58.546451+03:00","company":5,"shop":{"id":8,"name":"Магазин 2","address":{"city":"Махачкала ","region":"Дагестан","street":"Шамиля","country":"Россия"},"phone":"8 800 400 40 40","created_by":16},"position":{"id":10,"name":"Директор","created_by":16},"employee":18,"created_by":16}]');

describe('test EmployeesIndex', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(EmployeesIndex, {
      localVue,
      computed: {
        filteredEmployees(){
          return filteredEmployees
        },
        positions(){
          return positions
        },
        shops(){
          return shops
        },
        works(){
          return works
        }
      },
      methods: {
        getEmployees() { return { status: 200} },
        getListEmployees(){
          this.loading = false
        }
      },
      mocks: {
        $store: {
          dispatch: jest.fn(),
        },
      },
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
  });

  it('test search', () => {
    wrapper.find('.search-input').setValue('Серге');
    const expectedItem = JSON.parse('[{"id":17,"current_work":{"id":17,"shop":8,"position":12,"is_cashier":2,"salary":50000},"user":{"id":19,"email":"cashier2@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:28:36.050926+03:00"},"name":"Сергеева Инна Вячеславовна","phone":"+7 888 800 80 80","gender":"FEMALE","document":{"number":"3154851","serial":"7974","issued_by":"Россия"},"birthday":"2000-07-09","created_at":"2019-10-30T16:28:36.258738+03:00","created_by":16}]');

    expect(wrapper.vm.filteredItems).toEqual(expectedItem);
  });

  it('test select shop', () => {
    wrapper.setMethods({
      onShopChange: jest.fn()
    });
    const selectShop = wrapper.find('.select');
    const options = selectShop.findAll('option');
    options.at(2).setSelected();
    selectShop.trigger('input');

    expect(wrapper.vm.onShopChange).toHaveBeenCalledWith('7');
  });

  it('test computed filteredItems', () => {
    const localThis = {
      filteredEmployees,
      searchChar: 'фаин'
    };
    const expectItems = JSON.parse('[{"id":16,"current_work":{"id":16,"shop":7,"position":12,"is_cashier":2,"salary":50000},"user":{"id":18,"email":"cashier@mail.ru","is_superuser":false,"is_active":true,"date_joined":"2019-10-30T16:27:19.200017+03:00"},"name":"Ахмедханова Фаина Исламовна","phone":"+7 777 456 78 90","gender":"FEMALE","document":{"number":"474575","serial":"7847","issued_by":"Россия"},"birthday":"2000-10-01","created_at":"2019-10-30T16:27:19.425571+03:00","created_by":16}]');
    const result = EmployeesIndex.computed.filteredItems.call(localThis);

    expect(result).toEqual(expectItems);
  });

  it('test method onSearchChange', () => {
    const localThis = {
      searchChar: ''
    };
    EmployeesIndex.methods.onSearchChange.call(localThis, 'searchText');

    expect(localThis.searchChar).toBe('searchText');
  });

  it('test method onShopChange', () => {
    const localThis = {
      $store: {
        dispatch: jest.fn()
      }
    };
   EmployeesIndex.methods.onShopChange.call(localThis, 1);

   expect(localThis.$store.dispatch).toHaveBeenCalledWith('searchEmployeeByShop', 1);
  });

  it('test method getListEmployees', async() => {
    const localThis = {
      loading: '',
      $store: {
        dispatch(){
          return {
            status: 200
          }
        }
      }
    };
    await EmployeesIndex.methods.getListEmployees.call(localThis);

    expect(localThis.loading).toBeFalsy();
  });

  it('test method getListEmployees', async() => {
    const localThis = {
      loading: '',
      $store: {
        dispatch(){
          return {
            status: 400
          }
        }
      }
    };
    await EmployeesIndex.methods.getListEmployees.call(localThis);

    expect(localThis.loading).toBeTruthy();
  });

});
