import {createLocalVue, mount, RouterLinkStub} from '@vue/test-utils';
import Index from '@/pages/cashboxes/Index';
import SearchField from '@/components/elements/SearchField';
import LinkButton from '@/components/elements/LinkButton';
import ImageButton from '@/components/elements/ImageButton';
import ActionButton from '@/components/elements/ActionButton';
import Table from '@/components/cashboxes/Table';
import Select from '@/components/elements/Select';
import VModal from 'vue-js-modal';
import Vuex from 'vuex';
import VeeValidate from 'vee-validate'

const localVue = createLocalVue();

localVue.use(VModal);
localVue.use(Vuex);
localVue.use(VeeValidate, {
  inject: false
});

const store = new Vuex.Store({
  getters: {
    shops() { return shops },
    cashboxes() { return cashboxes }
  },
  actions: {
    getCashboxes: jest.fn(),
    getListShops: jest.fn(),
    getShopCashboxes: jest.fn()
  }
});

const shops = JSON.parse('[{"id":2,"name":"Магазин 2","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Шамиля ","country":"Россия"},"phone":"8  555 400 40 40","created_by":2},{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2}]');
const cashboxes = JSON.parse('[{"id":13,"balance":null,"name":"Касса 0","created_at":"2019-09-30T15:04:30.870281+03:00","shop":{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2},"created_by":3},{"id":3,"balance":"0.00","name":"Касса 3","created_at":"2019-09-25T18:01:43.622476+03:00","shop":{"id":2,"name":"Магазин 2","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Шамиля ","country":"Россия"},"phone":"8  555 400 40 40","created_by":2},"created_by":3},{"id":2,"balance":null,"name":"Касса 2","created_at":"2019-09-11T12:50:27.011356+03:00","shop":{"id":2,"name":"Магазин 2","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Шамиля ","country":"Россия"},"phone":"8  555 400 40 40","created_by":2},"created_by":2},{"id":1,"balance":"172.00","name":"Касса 1","created_at":"2019-09-11T12:50:20.198954+03:00","shop":{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2},"created_by":2}]');

describe('text CashBoxIndex', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(Index, {
      localVue,
      store,
      mocks: {
        $format: {
          balance() {
            return ''
          }
        },
        $date: {
          toDateTime(){
            return ''
          }
        },
        $eventBus: {
          $on: jest.fn(),
          $off: jest.fn()
        },
      },
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
  });

  it('searchChar is `Крон` ', () => {
    wrapper.find('.search-input').setValue('Крон');
    wrapper.find(SearchField).trigger('input');
    expect(wrapper.vm.searchChar).toBe('Крон');
  });

  it('test search with value `Магазин 1` ', () => {
    wrapper.find('.search-input').setValue('Магазин 1');
    wrapper.find(SearchField).trigger('input');
    const tableTextContent = wrapper.find(Table).text();

    expect(wrapper.element.querySelectorAll('.table > tbody > tr')).toHaveLength(2);
    expect(tableTextContent).toMatch('Магазин 1');
    expect(tableTextContent).not.toMatch('Магазин 2');
  });

  it('called getShopCashboxes after select shop', () => {
    wrapper.setMethods({ $store: { dispatch: jest.fn()  }} );
    wrapper.find('.select').setValue('Магазин 2');
    wrapper.find(Select).trigger('input');
    expect(wrapper.vm.$store.dispatch).toHaveBeenCalled();
  });

  it('show modal NewCashBox', () => {
    wrapper.find(LinkButton).trigger('click');
    expect(wrapper.contains('[data-modal="info-cashbox"]')).toBeTruthy();
  });

  it('show modal DeleteCashBox', () => {
    wrapper.find(ImageButton).trigger('click');
    expect(wrapper.contains('[data-modal="delete-cashbox"]')).toBeTruthy();
  });

  it('show modal NewCashBox', () => {
    wrapper.find(ActionButton).trigger('click');
    expect(wrapper.contains('[data-modal="new-cashbox"]')).toBeTruthy();
  });

  it('test computed genStoreName', () => {
    const localThis = { cashboxes: { length: 1 } };
    expect(Index.computed.genStoreName.call(localThis)).toBe('Магазин 2');
  });

  it('test computed filteredItems with `МагАзиН 1` ', () => {
    const expectArray = JSON.parse('[{"id":13,"balance":null,"name":"Касса 0","created_at":"2019-09-30T15:04:30.870281+03:00","shop":{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2},"created_by":3},{"id":1,"balance":"172.00","name":"Касса 1","created_at":"2019-09-11T12:50:20.198954+03:00","shop":{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2},"created_by":2}]');
    const localThis = {
      cashboxes: cashboxes,
      searchChar: 'МагАзиН 1'
    };
    expect(Index.computed.filteredItems.call(localThis)).toEqual(expectArray);
  });

  it('test computed filteredItems with `КАсСа 0`', () => {
    const expectArray = JSON.parse('[{"id":13,"balance":null,"name":"Касса 0","created_at":"2019-09-30T15:04:30.870281+03:00","shop":{"id":1,"name":"Магазин 1","address":{"city":"Махачкала","region":"Дагестан","street":"пр. Ирчи Казака ","country":"Россия"},"phone":"8  777 800 80 80","created_by":2},"created_by":3}]');
    const localThis = {
      cashboxes: cashboxes,
      searchChar: 'КАсСа 0'
    };
    expect(Index.computed.filteredItems.call(localThis)).toEqual(expectArray);
  });

  it('test method onSearchChange', () => {
    const localThis = {
      searchChar: ''
    };
    Index.methods.onSearchChange.call(localThis, 'text');
    expect(localThis.searchChar).toBe('text');
  });

  it('called methods getShopCashboxes', () => {
    const localThis = {
      $store: { dispatch: jest.fn() }
    };
    Index.methods.onShopChange.call(localThis, 'text');
    expect(localThis.$store.dispatch).toHaveBeenCalledWith('getShopCashboxes', 'text');
  });

  it('no called methods getShopCashboxes', () => {
    const localThis = {
      $store: { dispatch: jest.fn() }
    };
    Index.methods.onShopChange.call(localThis, null);
    expect(localThis.$store.dispatch).not.toHaveBeenCalled();
  });

});
