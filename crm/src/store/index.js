import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import clients from './modules/clients'
import employees from './modules/employees'
import moves from './modules/moves'
import procurement from './modules/procurement'
import procurements from './modules/procurements'
import products from './modules/products'
import providers from './modules/providers'
import returns from './modules/returns'
import sales from './modules/sales'
import searchProducts from './modules/searchProducts'
import settings from './modules/settings'
import shops from './modules/shops'
import discounts from './modules/discounts'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    clients,
    employees,
    moves,
    procurement,
    procurements,
    products,
    providers,
    returns,
    sales,
    searchProducts,
    settings,
    shops,
    discounts
  },
  strict: process.env.NODE_ENV !== 'production'
});
