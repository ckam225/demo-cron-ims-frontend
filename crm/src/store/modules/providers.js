/* eslint-disable */
import api from '@/api';


export default {
  state: {
    providers: [],
  },
  mutations: {
    SET_PROVIDERS: (state, payload) => state.providers = payload,
  },
  actions: {
    async fetchProviders({commit}) {
      try {
        const response = await api.get(`/providers/`);
        commit('SET_PROVIDERS', response.data);
      } catch (e) {
        return e.response
      }
    },

    async createProvider({dispatch}, payload) {
      try {
        const status = await api.post(`/providers/`, payload);
        dispatch('fetchProviders');
        return status
      } catch (e) {
        return e.response
      }
    },

    async deleteProvider({dispatch}, id){
      try {
        const response = await api.delete(`/providers/${id}/`);
        dispatch('fetchProviders');
        return response
      } catch (e) {
        return e.response
      }
    }
  },
  getters: {
    providers: state => state.providers,
  }
}
