import api from '@/api';
import Vue from 'vue'

function setRangeMoves(moves) {
    const ranges = []
    moves.forEach((move) => {
      const date = new Vue().$date.toDateFormat(move.created_at)
      const range = ranges.find(r => r.date === date)
      if(range != null){
        range.entries.push(move)
      }else{
        ranges.push({date: new Vue().$date.toDateFormat(move.created_at), entries: [move]})
      }
    })
    return ranges
}

export default {
  namespaced: true,
  state: {
   moves: [],
   rangeMoves: [],
   movePositions:[]
  },
  mutations: {
    SET_MOVES: (state, payload) => {
      state.moves = payload
      state.rangeMoves = setRangeMoves(payload)
    },
    ADD_MOVE: (state, payload) => {
      state.moves.push(payload)
      state.rangeMoves = setRangeMoves(state.moves)
    },
    CLEAR_MOVE_POSITIONS: (state) => state.movePositions = [],
    SEARCH_MOVES: (state, payload) => {
      state.rangeMoves = setRangeMoves(payload)
    },
    SEARCH_MOVES_BY_ID: (state, payload) => {
      if(payload === ''){
        state.rangeMoves = setRangeMoves(state.moves)
      }
      else{
        const result = state.moves.filter(m => m.id === parseInt(payload))
        state.rangeMoves = setRangeMoves(result)
      }
    },
    SEARCH_MOVES_BY_PERIOD: (state, payload) => {
      if(payload[0] === null || payload[1] === null){
        state.rangeMoves = setRangeMoves(state.moves)
      }
      else{
        const result = state.moves.filter((m) => {
          return new Date(m.created_at) >= new Date(payload[0]) &&
                 new Date(m.created_at) <= new Date(payload[1])
        })
        state.rangeMoves = setRangeMoves(result)
     }
    },
    ADD_POSITION: (state, payload) => {
       if(!state.movePositions.find(mp => mp.product === parseInt(payload.product))){
        let count =  payload.count || 0;
        state.movePositions.push({product: payload.product, name: payload.name, count: count, saved: false })
       }
    },
    UPDATE_POSITION: (state, payload) => {
      const position = state.movePositions.find(mp => mp.product === parseInt(payload.id))
      if(position){
        position.count =  parseInt(payload.qte)
      }
    },
    REMOVE_POSITION: (state, index) => {
      state.movePositions.splice(index, 1)
    }
  },
  actions: {
    async fetchMoves({commit}){
      try {
          const response = await api.get(`/moves/`)
          commit("SET_MOVES", response.data)
          return response
      } catch (e) {
        return e.response
      }
    },
    async getMove(context, id){
      try {
          const response = await api.get(`/moves/${id}/`)
          return response
      } catch (e) {
        return e.response
      }
    },
    async submitNewMove({commit}, form){
      try {
          const response = await api.post(`/moves/`, form)
          commit("ADD_MOVE", response.data)
          commit('CLEAR_MOVE_POSITIONS')
          return response
      } catch (e) {
        return e.response
      }
    },
    async confirmMove(context, id){
      try {
        return await api.put(`/moves/${id}/set_posted/`)
      } catch (e) {
        return e.response
      }
    },
    async deleteMove(context, id){
      try {
        const response = await api.delete(`/moves/${id}/`)
        return response
      } catch (e) {
        return e.response
      }
    },
    async searchMoves({commit}, searchParams){
      try {
          const response = await api.get(`/moves/`, {params: searchParams})
          commit("SEARCH_MOVES", response.data)
          return response
      } catch (e) {
        return e.response
      }
    },
    async deleteMoveLine(context, id){
      try {
        const response = await api.delete(`/moveLines/${id}/`)
        return response
      } catch (e) {
        return e.response
      }
    },
    async updateMove(context, [form, id]){
      try {
         const response = await api.put(`/moves/${id}/`, form);
         context.commit('CLEAR_MOVE_POSITIONS');
         return response;
      } catch (e) {
        return e.response
      }
    },
    searchMovesById({commit}, searchValue){
      commit("SEARCH_MOVES_BY_ID", searchValue)
    },
    searchMovesByPeriod({commit}, period){
      commit("SEARCH_MOVES_BY_PERIOD", period)
    },
    addToPositions({commit}, product){
      commit("ADD_POSITION", product)
    },
    updatePosition({commit}, payload){
      commit("UPDATE_POSITION", payload)
    },
    removeFromPositions({commit}, product){
      commit("REMOVE_POSITION", product)
    }
  },
  getters: {
    moves: (state) => state.moves,
    rangeMoves: (state) => state.rangeMoves,
    movePositions: (state) => state.movePositions
  }
}
