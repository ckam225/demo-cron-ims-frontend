import api from '@/api';

export default {
  namespaced: true,
  state: {
    products: [],
    categories: [],
    dynamic_discounts: [],
    static_discounts: [],
  },
  mutations: {
    SET_PRUDUCT_DISCOUNTS: (state, payload) => state.products = payload,
    ADD_PRODUCT_DISCOUNT: (state, payload) => state.products.push(payload),
    SET_CATEGORY_DISCOUNTS: (state, payload) => state.categories = payload,
    ADD_CATEGORY_DISCOUNT: (state, payload) => state.categories.push(payload),
    SET_DYNAMIC_DISCOUNTS: (state, payload) => state.dynamic_discounts = payload,
    ADD_DYNAMIC_DISCOUNT: (state, payload) => state.dynamic_discounts.push(payload),
    SET_STATIC_DISCOUNTS: (state, payload) => state.static_discounts = payload,
    ADD_STATIC_DISCOUNT: (state, payload) => state.static_discounts.push(payload),
  },
  actions: {
    async fetchProductDiscounts({commit}){
      try {
        const response = await api.get(`/productsdiscounts/`);
        commit('SET_PRUDUCT_DISCOUNTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async createProductDiscount({commit}, form) {
        try {
          const response = await api.post(`/productsdiscounts/`, form);
          commit('ADD_PRODUCT_DISCOUNT', response.data);
          return response;
        } catch (e) {
          return e.response;
        }
    },
    async fetchCategoryDiscounts({commit}){
      try {
        const response = await api.get(`/categoriesdiscounts/`);
        commit('SET_CATEGORY_DISCOUNTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async createCategoryDiscount({commit}, form) {
        try {
          const response = await api.post(`/categoriesdiscounts/`, form);
          commit('ADD_CATEGORY_DISCOUNT', response.data);
          return response;
        } catch (e) {
          return e.response;
        }
    },
    async fetchDynamicDiscounts({commit}){
      try {
        const response = await api.get(`/settingsdynamicdiscounts/`);
        commit('SET_DYNAMIC_DISCOUNTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async createDynamicDiscount({commit}, form) {
        try {
          const response = await api.post(`/settingsdynamicdiscounts/`, form);
          commit('ADD_DYNAMIC_DISCOUNT', response.data);
          return response;
        } catch (e) {
          return e.response;
        }
    },

    async fetchStaticDiscounts({commit}){
      try {
        const response = await api.get(`/settingsstaticdiscounts/`);
        commit('SET_STATIC_DISCOUNTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async createStaticDiscount({commit}, form) {
        try {
          const response = await api.post(`/settingsstaticdiscounts/`, form);
          commit('ADD_STATIC_DISCOUNT', response.data);
          return response;
        } catch (e) {
          return e.response;
        }
    },
    async addStaticClientDiscount(context, form){
      try {
        const response = await api.post(`/staticclientdiscounts/`, form);
        return response;
      } catch (e) {
        return e.response; 
      }
    }
  },
  getters: {
    products: (state) => state.products,
    categories: (state) => state.categories,
    dynamic_discounts: (state) => state.dynamic_discounts,
    static_discounts: (state) => state.static_discounts,
  }
};