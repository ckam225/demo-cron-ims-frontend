/* eslint-disable */
import api from '@/api';


export default {
  state: {
    elements: [],
    searchId: '',
    arrayDate: [],
    shopId: ''
  },
  mutations: {
    SET_PROCUREMENTS: (state, payload) => state.elements = payload,
    SET_SEARCHID: (state, id) => state.searchId = id,
    SET_SHOPID: (state, id) => state.shopId = id,
    SET_ARRAYDATE: (state, [dateStart, dateEnd]) => state.arrayDate = [dateStart, dateEnd],
  },
  actions: {
    async fetchProcurements({commit, state}) {
      const params = {};
      if (typeof state.searchId != 'undefined' && state.searchId)
        params.number = state.searchId;
      if (typeof state.shopId != 'undefined' && state.shopId)
        params.shop = state.shopId;
      if (typeof state.arrayDate[0] != 'undefined' && state.arrayDate[0])
        params.created_at__gte = state.arrayDate[0];
      if (typeof state.arrayDate[1] != 'undefined' && state.arrayDate[1])
        params.created_at__lte = state.arrayDate[1];
      try {
        const response = await api.get(`/procurements/`, {
          params: params
        });

        const dates = [];
        for await (const element of response.data) {
          const cropIndex = element.created_at.indexOf('T');
          const cropDate = element.created_at.substr(0, cropIndex);
          const index = dates.map(x => x.date).indexOf(cropDate);
          if (index !== -1) {
            dates[index].procurements.push(element)
          } else {
            const obj = {};
            obj.date = cropDate;
            obj.procurements = [element];
            dates.push(obj)
          }
        }
        commit('SET_PROCUREMENTS', dates)
      } catch (e) {
        return e.response
      }
    },
    setArrayDate({commit}, payload) {
      let yearEnd = payload.dateEnd.getFullYear();
      if (yearEnd === 1970) {
        payload.dateEnd = new Date();
      }
      commit('SET_ARRAYDATE', [payload.dateStart, payload.dateEnd]);
    },
    async setSearchId({commit}, payload) {
      commit('SET_SEARCHID', payload.id);
    },
    async setShopId({commit}, payload) {
      commit('SET_SHOPID', payload.id);
    },
  },
  getters: {
    elements: ({elements}) => elements,
    searchId: ({searchId}) => searchId,
    arrayDate: ({arrayDate}) => arrayDate,
  }
};
