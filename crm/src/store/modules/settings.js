
import api from '@/api';
import storage from '@/plugins/storage';


export default {
  namespaced: true,
  state: {
    company: {
      tariff: 1,
      name: '',
      inn: '',
      address: {
        country: '',
        region: '',
        city: '',
        street: ''
      }
    },
    loyaltyProgram: null,
  },
  mutations: {
    SET_COMPANY: (state, payload) => {
      storage.set('company', payload);
      state.company = payload;
    },
    SET_LOYALTY_PROGRAM: (state, payload) => {
      if(Array.isArray(payload)){
        state.loyaltyProgram = payload[0];
      }else{
        state.loyaltyProgram = payload;
      }
    }
  },
  actions: {
    async getCompany({commit}) {
      try {
        const response = await api.get(`/companies/`);
        commit('SET_COMPANY', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async updateCompany({commit}, form) {
      try {
        const response = await api.put(`/companies/${form.id}/`, form);
        commit('SET_COMPANY', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async getLoyaltyProgram(context) {
      try {
        const response = await api.get(`/settingsloyaltyprograms/`);
        context.commit('SET_LOYALTY_PROGRAM', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async updateLoyaltyProgram(context, form) {
      try {
        const response = await api.put(`/settingsloyaltyprograms/${context.state.loyaltyProgram.id}/`, form);
        context.commit('SET_LOYALTY_PROGRAM', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
  },
  getters: {
    company: state => state.company,
    loyaltyProgram: state => state.loyaltyProgram,
  }
}
