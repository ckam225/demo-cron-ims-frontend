import api from '@/api';
import storage from '@/plugins/storage';

export default {
  state: {
    shops: [],
    cashboxes: [],
    shifts: [],
    shift: storage.get('shift')
  },
  mutations: {
    SET_SHOPS: (state, payload) => state.shops = payload,
    SET_CASHBOXES: (state, payload) => state.cashboxes = payload,
    ADD_SHOP: (state, payload) => state.shops.push(payload),
    ADD_CASHBOX: (state, payload) => state.cashboxes.push(payload),
    SET_SHIFTS: (state, payload) => state.shifts = payload,
    ADD_SHIFT: (state, payload) => {
      const cashbox = state.cashboxes.find(c => c.id === payload.cashbox);
      const s = { ...payload, cashboxe: cashbox };
      storage.set('shift', s);
      state.shift = s;
      state.shifts.push(payload);
    },
    CLOSE_SHIFT: (state) => {
      storage.remove('shift');
      state.shift = null;
    },
    UPDATE_SHIFT: (state, amount, is_insertion = true) => {
      if (is_insertion) {
        state.shift.revenue = parseFloat(state.shift.revenue) + parseFloat(amount);
      } else {
        state.shift.revenue = parseFloat(state.shift.revenue) - parseFloat(amount);
      }
      storage.set('shift', state.shift);
    },
    REFRESH_SHIFT: (state, payload) => {
      if (payload.closed_at !== null) {
        storage.remove('shift');
        state.shift = null;
        location.reload();
      }
      state.shift = payload;
      storage.set('shift', payload);
    },
  },
  actions: {
    async getListShops({ commit }) {
      try {
        const response = await api.get(`/shops/`);
        commit('SET_SHOPS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async getListShopsWithStats({commit, dispatch}) {
      try {
        commit('SET_SHOPS', []);
        const response = await api.get('/shops/');

        for (let shop of response.data) {
          const shopStats = await dispatch('getShopStats', shop.id);
          if (shopStats.status === 200)
            shop = {...shop, allStats: shopStats.data};
          else
            shop = {...shop, allStats: null};

          const shopStatsToday = await dispatch('getShopStatsByPeriod', {id: shop.id, period: 'day'});
          if (shopStatsToday.status === 200)
            shop = {...shop, dailyStats: shopStatsToday.data};
          else
            shop = {...shop, dailyStats: null};

          commit('ADD_SHOP', shop);
        }

        return response;
      } catch (e) {
        return e.response;
      }
    },
    async getShopsById(context, id) {
      try {
        return await api.get(`/shops/${id}/`);
      } catch (e) {
        return e.response;
      }
    },
    async refreshShift({ commit }, id) {
      try {
        const response = await api.get(`/shifts/${id}/`);
        commit('REFRESH_SHIFT', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async getCashboxes({ commit }) {
      try {
        const response = await api.get('/cashboxes/');
        commit('SET_CASHBOXES', response.data);
        return response.data;
      } catch (e) {
        return e.response;
      }
    },
    async getShopCashboxes({ commit }, id) {
      try {
        const response = await api.get('/cashboxes/', {
          params: {
            shop: id
          }
        });
        commit('SET_CASHBOXES', response.data);
        return response.data;
      } catch (e) {
        return e.response;
      }
    },
    async getShopStats(context, id) {
      try {
        return await api.get(`/shops/${id}/get_statistics/`);
      } catch (e) {
        return e.response
      }
    },
    async getShopStatsByPeriod(context, {id, period}) {
      try {
        return await api.get(`/shops/${id}/get_statistics/`, {
          params: {
            period: period
          }
        });
      } catch (e) {
        return e.response
      }
    },
    async getShopDailyStats(context, filterParams) {
      try {
        const response = await api.get(`/shops/get_daily_statistics/`, {params: filterParams});
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async getShopWeeklyStats(context, filterParams) {
      try {
        const response = await api.get(`/shops/get_weekly_statistics/`, {params: filterParams });
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async getShopMonthlyStats(context, filterParams) {
      try {
        const response = await api.get(`/shops/get_monthly_statistics/`, {params: filterParams});
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async getListShifts({commit}, searchParams) {
      try {
        const response = await api.get(`/shifts/`, {params: searchParams});
        commit('SET_SHIFTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async submitNewShop({ dispatch }, form) {
      try {
        const response = await api.post(`/shops/`, form);
        if (response.status === 201) {
          dispatch('getListShopsWithStats');
        }
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async submitNewCashbox({ commit }, form) {
      try {
        const status = await api.post(`/cashboxes/`, form);
        commit('ADD_CASHBOX', status.data);
        return status;
      } catch (e) {
        return e.response;
      }
    },
    async submitNewShift({ commit }, form) {
      try {
        const status = await api.post(`/shifts/`, form);
        commit('ADD_SHIFT', status.data);
        return status;
      } catch (e) {
        return e.response;
      }
    },
    async closeShift({ dispatch, getters }, shiftId) {
      try {
        const id = shiftId ? parseInt(shiftId) : getters.shift.id;
        const response = await api.patch(`/shifts/${id}/`);
        dispatch('getListShifts');
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async updateShop({dispatch}, {id, form}) {
      try {
        const status = await api.put(`/shops/${id}/`, form);
        dispatch('getListShops');
        return status
      } catch (e) {
        return e.response
      }
    },
    async deleteCashbox({ dispatch }, cashbox) {
      try {
        const status = await api.delete(`/cashboxes/${cashbox.id}/`);
        dispatch('getCashboxes');
        return status;
      } catch (e) {
        return e.response;
      }
    },
    async submitNewInsertion({ commit, dispatch }, form) {
      try {
        const response = await api.post(`/insertions/`, form);
        if (response.status === 201) {
          commit('UPDATE_SHIFT', response.data.amount);
          dispatch('getListShifts');
        }
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async submitNewWithdrawals({ commit, dispatch }, form) {
      try {
        const response = await api.post(`/withdrawals/`, form);
        if (response.status === 201) {
          commit('UPDATE_SHIFT', response.data.amount, false);
          dispatch('getListShifts');
        }
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async getOpenShift(context, cashboxId) {
      try {
        return await api.get(`/cashboxes/${cashboxId}/get_open_shift/`)
      } catch (e) {
        return e.response;
      }
    },
    async deleteShop(context, shopId) {
      try {
        return await api.delete(`/shops/${shopId}/`)
      } catch (e) {
        return e.response;
      }
    }
  },
  getters: {
    shops: state => state.shops,
    cashboxes: state => state.cashboxes,
    shifts: state => state.shifts,
    shift: state => state.shift
  }
}
