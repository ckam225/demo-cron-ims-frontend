import '@/api';
import productsStore from '@/store/modules/products.js';

import {createLocalVue} from '@vue/test-utils';
import Vuex from 'vuex';


const {mutations, getters} = productsStore;
const breadcrumbs = JSON.parse('[{"id":3,"name":"Компьютеры"},{"id":5,"name":"Видеокарты"},{"id":11,"name":"AMD"}]');
const lastBreadCrumb = JSON.parse('[{"id":11,"name":"AMD"}]');

const breadcrumbsAfterRemovedItem = JSON.parse('[{"id":3,"name":"Компьютеры"},{"id":5,"name":"Видеокарты"}]');
const breadcrumb = JSON.parse('{"id":3,"name":"Компьютеры"}');

const localVue = createLocalVue();
localVue.use(Vuex);


describe('add Breadcrumbs', () => {

  const store = new Vuex.Store({
    state: {
      breadcrumbs: []
    },
    mutations,
    getters
  });

  test('empty breadcrumbs', () => {
    expect(store.state.breadcrumbs).toEqual([]);
  });

  test('add breadcrumb', () => {
    store.commit('ADD_BREADCRUMB', breadcrumb);
    expect(store.state.breadcrumbs).toContain(breadcrumb);
  });

});


describe('remove/truncate breadcrumbs ', () => {

  const store = new Vuex.Store({
    state: {
      breadcrumbs: breadcrumbs
    },
    mutations,
    getters
  });

  test('remove breadcrumbs', () => {
    store.commit('REMOVE_BREADCRUMBS', 5);
    expect(store.state.breadcrumbs).toEqual(expect.arrayContaining(breadcrumbsAfterRemovedItem));
    expect(store.state.breadcrumbs).not.toEqual(expect.arrayContaining(lastBreadCrumb));
  });

  test('truncate breadcrumbs', () => {
    store.commit('TRUNCATE_BREADCRUMBS');
    expect(store.state.breadcrumbs).toEqual([]);
  });

});
