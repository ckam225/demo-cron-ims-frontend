import api from '@/api';
import storage from '@/plugins/storage';

export default {
  state: {
    user: {
      token: null,
      meta: null
    }
  },
  mutations: {
    SET_AUTH_IN(state, payload) {
      state.user.token = payload.token;
      state.user.meta = payload.meta;
      storage.set('user', state.user);
    },
    SET_AUTH_OUT() {
      storage.reset();
      location.reload();
    },
    SET_USER_TOKEN(state, payload) {
      state.user.token = payload.auth_token;
      storage.set('user', state.user);
    },
    SET_USER_META(state, meta) {
      state.user.meta = meta;
      storage.set('user', state.user);
    },
  },
  actions: {
    async login({commit}, form) {
      try {
        const response = await api.post('/auth/token/login/', form);
        commit('SET_USER_TOKEN', response.data);
        return response;
      } catch (e) {
        storage.reset()
        return e.response;
      }
    },
    async register({commit}, form) {
      try {
        let response = await api.post('/auth/users/', form);
        const user =  {meta: response.data};
        response = await api.post('/auth/token/login/', form);
        commit('SET_AUTH_IN', { ...user, token: response.data.auth_token});
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async me({commit}) {
      try {
        let response = await api.get(`/auth/users/me/`);
        response = await api.get(`/auth/users/${response.data.id}/`);
        commit('SET_USER_META', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async logout({commit}) {
      try {
        const response = await api.post(`/auth/token/logout/`);
        if (response.status !== 404) {
          commit('SET_AUTH_OUT');
        }
        return response;
      } catch (error) {
        return error.response;
      }
    },
    async resetPassword({commit}, form) {
      try {
        const response = await api.post(`auth/users/set_password/`, form);
        if (response.status === 204) {
          commit('SET_AUTH_OUT');
        }
        return response;
      } catch (error) {
        return error.response;
      }
    },
    async restorePassword(context, form) {
      try {
        const response = await api.post(`auth/users/reset_password/`, form);
        return response;
      } catch (error) {
        return error.response;
      }
    },
    async resetPasswordConfirm(context, form){
      try{
        const response = await api.post(`auth/users/reset_password_confirm/`, form);
        return response;
      } catch (error) {
        return error.response;
      }
    }
  },
  getters: {
    isLoggedIn: state => !!state.user.token,
    user: state => state.user
  }
}
