import api from '@/api';
import Vue from 'vue'

function round(x, accuracy) {
  if (x === '') return null;
  const EPSILON = 0.0001;
  if (accuracy < EPSILON)
    accuracy = EPSILON;
  // Due to inaccuracy of float numbers, we invert "accuracy"
  // Example: x = 11.71, accuracy = 0.1, the answer is supposed to be 11.7
  // Performing Math.round(11.71*10)/10 gives us 11.7 instead
  // of Math.round(11.71/0.1)*0.1 that would give 11.700000000000001
  // For small invertEpsilon such as {0.5  0.1  0.05  0.01},
  // it works just fine since the rounded dividend is integer
  const invertEpsilon = 1 / accuracy;
  return Math.round(x * invertEpsilon) / invertEpsilon;
}

export default {
  state: {
    procurement: {},
    newPositions: [],
    roundingAccuracy: 1
  },
  mutations: {
    SET_PROCUREMENT: (state, payload) => state.procurement = payload,
    RESET_PROCUREMENT: (state) => state.procurement = {},
    RESET_NEWPOSITIONS: (state) => state.newPositions = [],
    SET_NEWPOSITION: (state, payload) => {
      const exist = state.newPositions.findIndex(p => p.id === payload.product) >= 0
      if(!exist)
        state.newPositions.push(payload)
    },
    UPDATE_NUMBER_PROPERTY_OF_NEWPOSITION:
      (state, [index, property, value]) => state.newPositions[index][property] = Number(value),
    UPDATE_AND_ROUND_NUMBER_PROPERTY_OF_NEWPOSITION:
      (state, [index, property, value]) => state.newPositions[index][property] = round(value, state.roundingAccuracy),
    ADD_MARGIN_TO_PRICE_PROPERTY_OF_NEWPOSITIONS: (state, [priceProperty, margin]) => {
      if (!['wholesale', 'retail'].includes(priceProperty))
        throw new Error('Unknown priceProperty, expected "wholesale" or "retail" but got ' + priceProperty);
      const {roundingAccuracy} = state;
      state.newPositions.forEach(newPosition => {
        if (newPosition.purchases) {
          const purchases = newPosition.purchases || 0;
          newPosition[priceProperty] = round(purchases * margin + purchases, roundingAccuracy);
        }
      });
    },
    SET_ROUNDING_ACCURACY_FOR_PRICES_OF_NEWPOSITIONS: (state, {roundingAccuracy}) => {
      if (!roundingAccuracy) {
        state.roundingAccuracy = 0;
        return;  // no rounding
      }
      roundingAccuracy = Number(roundingAccuracy);
      state.newPositions.forEach(newPosition => {
        if (newPosition.wholesale)
          newPosition.wholesale = round(newPosition.wholesale, roundingAccuracy);
        if (newPosition.retail)
          newPosition.retail = round(newPosition.retail, roundingAccuracy);
      });
      state.roundingAccuracy = roundingAccuracy;
    },
    DELETE_NEWPOSITION: (state, index) => state.newPositions.splice(index, 1),
    DELETE_POSITION: function (state, id) {
      const index = state.procurement.positions.map(x => x.id).indexOf(id);
      if (index !== -1) {
        state.procurement.positions.splice(index, 1)
      }
    }
  },
  actions: {
    async fetchProcurement({commit}, payload) {
      try {
        commit('RESET_NEWPOSITIONS');
        commit('RESET_PROCUREMENT');
        if (typeof payload.id !== 'undefined') {
          let url = `/procurements/${payload.id}`;
          const response = await api.get(url);
          commit('SET_PROCUREMENT', response.data)
        }
      } catch (e) {
        return e.response
      }
    },
    async setPosted({commit, state}) {
      try {
        const response = await api.put(`/procurements/${state.procurement.id}/set_posted/`);
        commit('SET_PROCUREMENT', response.data)
      } catch (e) {
        return e.response
      }
    },
    async deleteProcurementPosition({commit}, payload) {
      try {
        await api.delete(`/procurementlines/${payload.id}/`);
        commit('DELETE_POSITION', payload.id)
      } catch (e) {
        return e.response
      }
    },

    async deleteProcurement({commit}, payload) {
      try {
        const response = await api.delete(`/procurements/${payload.id}`);
        commit('RESET_PROCUREMENT');
        commit('RESET_NEWPOSITIONS');
        new Vue().$eventBus.$emit('procurement-deleted', response)
      } catch (e) {
        return e.response
      }
    },

    async submitNewProcurement({commit, state}, payload) {
      try {
        const data = {
          shop: parseInt(payload.shop, 10),
          provider: parseInt(payload.provider, 10),
          positions: state.newPositions
        };
        const response = await api.post(`/procurements/`, data);
        commit('SET_PROCUREMENT', response.data);
        commit('RESET_NEWPOSITIONS')
      } catch (e) {
        return e.response
      }
    },

    async editProcurement({commit, state}, payload) {
      try {
        const data = {
          shop: parseInt(payload.shop, 10),
          provider: parseInt(payload.provider, 10),
          positions: state.newPositions
        };
        const response = await api.put(`/procurements/${state.procurement.id}/`, data);
        commit('SET_PROCUREMENT', response.data);
        commit('RESET_NEWPOSITIONS')
      } catch (e) {
        return e.response
      }
    }

  },
  getters: {
    procurement: ({procurement}) => procurement,
    newPositions: ({newPositions}) => newPositions,
    roundingAccuracy: ({roundingAccuracy}) => roundingAccuracy
  }
};
