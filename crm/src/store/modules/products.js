import api from '@/api';
import storage from '@/plugins/storage';

import Vue from 'vue';


function deleteEmptyChildren(categories) {
  const lst = categories.slice();
  for (let i = 0; i < lst.length; i++) {
    if (lst[i].children.length > 0) {
      deleteEmptyChildren(lst[i].children);
    } else {
      delete lst[i].children;
    }
  }
  return lst;
}


function calculateSumCart(cart) {
  let sum = 0;
  const client = storage.get('client');
  cart.forEach(function (el) {
    const price = (client !== null && client.is_retail) ? el.price.retail : el.price.wholesale;
    sum = sum + Number(el.qte) * parseFloat(price);
  });
  return sum;
}


function filterCategories(categories) {
  const arr = [];
  categories.forEach(function (el) {
    if (!el.parent) {
      arr.push(el);
    }
  });
  return arr;
}


export default {
  state: {
    products: [],
    categories: [],
    cart: [],
    sumCart: 0,
    categories_filtered: [],
    breadcrumbs: []
  },
  mutations: {
    SET_PRODUCTS: (state, payload) => {
      state.products = payload;
    },
    UPDATE_PRODUCT_BALANCE: (state, payload) => {
      const product = state.products.find(p => p.id === payload.product);
      product.balance -= payload.count;
    },
    SET_CATEGORIES: (state, payload) => {
      state.categories = payload;
      state.categories_filtered = filterCategories(payload);
    },
    ADD_TO_CART: (state, payload) => {
      if (state.currentCategory !== null) {
        if (!payload.balance) {
          return;
        }
        const product = state.cart.find(p => p.id === payload.id);
        if (product) {
          if (product.qte < product.balance) {
            product.qte++;
          }
        } else {
          state.cart.push({...payload, qte: 1});
        }
        state.sumCart = calculateSumCart(state.cart);
      } else {
        new Vue().$toast.error('Пожалуйста, выберите клиента!');
      }

    },
    UPDATE_CART: (state, payload) => {
      const product = state.cart.find(p => p.id === payload.id);
      if (product && payload.qte <= product.balance) {
        product['qte'] = payload.qte;
      }
      state.sumCart = calculateSumCart(state.cart);
    },
    REMOVE_FROM_CART: (state, payload) => {
      const product = state.cart.find(p => p.id === payload.id);
      if (product) {
        state.cart.splice(payload.index, 1);
      }
      state.sumCart = calculateSumCart(state.cart);
    },
    TRUNCATE_CART: (state) => {
      state.cart = [];
      state.sumCart = 0;
    },
    FILTER_CATEGORIES: (state, payload) => {
      const categories = state.categories.find(p => p.id === payload.id);
      if (categories.children) {
        state.categories_filtered = categories.children;
      } else {
        state.categories_filtered = [];
      }
    },
    RESTORE_CATEGORIES: (state) => {
      state.categories_filtered = filterCategories(state.categories);
    },
    ADD_BREADCRUMB: (state, payload) => {
      state.breadcrumbs.push(payload);
    },
    REMOVE_BREADCRUMBS: (state, payload) => {
      for (let i = 0; i < state.breadcrumbs.length; i++) {
        if (state.breadcrumbs[i].id === payload) {
          state.breadcrumbs.splice(i + 1);
          break
        }
      }
    },
    TRUNCATE_BREADCRUMBS: (state) => {
      state.breadcrumbs.splice(0);
    }
  },
  actions: {
    filterCategories({commit}, payload) {
      commit('FILTER_CATEGORIES', payload);
    },
    restoreCategories({commit}) {
      commit('RESTORE_CATEGORIES');
    },
    async fetchProducts({commit}, id) {
      if (id === null) id = 'null';
      const config = {
        params: {
          category: id
        }
      };
      try {
        const response = await api.get(`/products/`, config);
        commit('SET_PRODUCTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async fetchProductsByShop({commit}) {
      try {
        const shift = storage.get('shift');
        if (!shift) return false;
        const response = await api.get(`/products/?shop=${shift.cashbox.shop.id}`);
        commit('SET_PRODUCTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async fetchCategories({commit}, categoryType) {
      const config = {};
      if (categoryType) {
        config.params = {
          category_type: categoryType
        }
      }
      try {
        const response = await api.get(`/categories/`, config);
        commit('SET_CATEGORIES', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async submitNewProduct({dispatch}, payload) {
      const config = {
        headers: {'Content-Type': 'multipart/form-data'}
      };

      try {
        const response = await api.post(`/products/`, payload.formData, config);
        dispatch('fetchProducts', payload.currentCategory);
        new Vue().$eventBus.$emit('added-new-product');
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async submitNewFolder({dispatch}, {form, categoryType}) {
      try {
        const response = await api.post(`/categories/`, form);
        dispatch('fetchCategories', categoryType);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async updateCategory({dispatch}, {id, form, categoryType}) {
      try {
        const response = await api.put(`/categories/${id}/`, form);
        dispatch('fetchCategories', categoryType);
        return response;
      } catch (e) {
        return e.response;
      }
    },

    async deleteCategory({dispatch}, {id, parentCategory, categoryType}) {
      try {
        const response = await api.delete(`/categories/${id}/`);
        dispatch('fetchCategories', categoryType);
        dispatch('fetchProducts', parentCategory);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async updateProduct({dispatch}, payload) {
      try {
        const response = await api.put(`/products/${payload.id}/`, payload.form);
        dispatch('fetchProducts', payload.parentCategory);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async deleteProduct({dispatch}, payload) {
      try {
        const response = await api.delete(`/products/${payload.id}/`);
        dispatch('fetchProducts', payload.categoryId);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async searchProduct({commit}, searchText) {
      const config = {
        params: {
          name__icontains: searchText
        }
      };
      try {
        const response = await api.get(`/products/`, config);
        commit('SET_PRODUCTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async addProductToCart({commit}, product) {
      commit('ADD_TO_CART', product);
    },
    async generateBarcode() {
      try {
        return await api.get(`/barcode`)
      } catch (e) {
        return e.response;
      }
    },
    async searchProductBy({commit}, payload) {
      try {
        const response = await api.get(`/products/`, {params: payload});
        if (payload.barcode && response.data.length === 1) {
          commit('ADD_TO_CART', response.data[0]);
        }
        commit('SET_PRODUCTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async searchProductsByCategory({commit}, payload) {
      try {
        const response = await api.get(`/products/`, {
          params: {
            shop: payload.shopId,
            category: payload.category.id,
            product_type: payload.category.product_type
          }
        });
        commit('SET_PRODUCTS', response.data);
        return response;
      } catch (e) {
        return e.response;
      }
    },
  },
  getters: {
    products: ({products}) => products,
    categories: ({categories}) => categories,
    categoriesWithoutEmptyChildren: ({categories}) => deleteEmptyChildren(categories),
    cart: ({cart}) => cart,
    sumCart: ({sumCart}) => sumCart,
    categories_filtered: ({categories_filtered}) => categories_filtered,
    breadcrumbs: ({breadcrumbs}) => breadcrumbs
  }
};
