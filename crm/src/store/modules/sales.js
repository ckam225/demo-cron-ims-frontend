/* eslint-disable */
import api from '@/api';

export default {
  state: {
    elementsForSales: [],
    saleSearchId: '',
    saleArrayDate: [],
    saleShopId: '',
    currentSale: {}
  },
  mutations: {
    SET_SALES: (state, payload) => state.elementsForSales = payload,
    SET_SALE: (state, payload) => state.currentSale = payload,
    SET_SALE_SEARCHID: (state, id) => state.saleSearchId = id,
    SET_SALE_SHOPID: (state, id) => state.saleShopId = id,
    SET_SALE_ARRAYDATE: (state, [dateStart, dateEnd]) => state.saleArrayDate = [dateStart, dateEnd],
  },
  actions: {
    async fetchSales({commit, state}) {
      const params = {};
      if (typeof state.saleSearchId != 'undefined' && state.saleSearchId)
        params.number = state.saleSearchId;
      if (typeof state.saleShopId != 'undefined' && state.saleShopId)
        params.shift__cashbox__shop = state.saleShopId;
      if (typeof state.saleArrayDate[0] != 'undefined' && state.saleArrayDate[0])
        params.created_at__gte = state.saleArrayDate[0];
      if (typeof state.saleArrayDate[1] != 'undefined' && state.saleArrayDate[1])
        params.created_at__lte = state.saleArrayDate[1];
      try {
        const response = await api.get(`/sales/`, {
          params: params
        });

        const dates = [];
        for await (const element of response.data) {
          const cropIndex = element.created_at.indexOf('T');
          const cropDate = element.created_at.substr(0, cropIndex);
          const index = dates.map(x => x.date).indexOf(cropDate);
          if (index !== -1) {
            dates[index].sales.push(element)
          } else {
            const obj = {};
            obj.date = cropDate;
            obj.sales = [element];
            obj.key = element.id;
            dates.push(obj)
          }
        }
        commit('SET_SALES', dates)
      } catch (e) {
        return e.response
      }
    },
    async saveReturn(context, state) {
      try {
        const response = await api.post('/returns/', state);
        return response.data;
      }catch (e) {
        return e.response;
      }
    },
    setSaleArrayDate({commit}, payload) {
      let yearEnd = payload.dateEnd.getFullYear();
      if (yearEnd === 1970) {
        payload.dateEnd = new Date();
      }
      commit('SET_SALE_ARRAYDATE', [payload.dateStart, payload.dateEnd]);
    },

    async setSaleSearchId({commit}, payload) {
      commit('SET_SALE_SEARCHID', payload.id);
    },

    async setSaleShopId({commit}, payload) {
      commit('SET_SALE_SHOPID', payload.id);
    },
    async fetchSale({commit}, payload) {
      try {
        if (typeof payload.id !== 'undefined') {
          const response = await api.get(`/sales/${payload.id}`);
          commit('SET_SALE', response.data)
        }
      } catch (e) {
        return e.response
      }
    },
    async saveSales(context, form) {
      try {
        const response = await api.post('/sales/', form)
        return response
      } catch (e) {
        return e.response
      }
    }
  },
  getters: {
    elementsForSales: ({elementsForSales}) => elementsForSales,
    currentSale: ({currentSale}) => currentSale
  }
};
