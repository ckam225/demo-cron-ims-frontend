/* eslint-disable */
import api from '@/api';
import {Tree} from 'vue-tree-list'


export default {
  state: {
    categoriesForSearch: {},
    productsForSearch: [],
    selectedCategoryForSearch: '',
    selectedNameForSearch: ''
  },

  mutations: {
    SET_CATEGORIES_FOR_SEARCH: (state, payload) => state.categoriesForSearch = payload,
    SET_PRODUCTS_FOR_SEARCH: (state, payload) => state.productsForSearch = payload,
    SET_SELECTED_CATEGORY_FOR_SEARCH: (state, payload) => state.selectedCategoryForSearch = payload,
    SET_SELECTED_NAME_FOR_SEARCH: (state, payload) => state.selectedNameForSearch = payload,
  },
  actions: {
    fetchCategoriesForSearch({commit}, payload) {

      function reWriteCategories(category) {
        const obj = {};
        obj.id = category.id;
        obj.name = category.name;
        obj.dragDisable = true;
        obj.addTreeNodeDisabled = true;
        obj.addLeafNodeDisabled = true;
        obj.editNodeDisabled = true;
        obj.delNodeDisabled = true;
        if (category.children && category.children.length) {
          obj.children = [];
          for (const child of category.children) {
            obj.children.push(reWriteCategories(child))
          }
        }
        return obj
      }

      try {
        const categoriesForSearch = [];
        for (const category of payload) {
          if (category.parent == null && category.category_type === 1) categoriesForSearch.push(reWriteCategories(category))
        }
        const tree = new Tree(categoriesForSearch);
        commit('SET_CATEGORIES_FOR_SEARCH', tree);
      } catch (e) {
        return e
      }
    },

    async fetchProductsForSearch({commit}, payload) {
      const config = {
        params: {
          category: payload.selectedCategoryForSearch,
          name__icontains: payload.selectedNameForSearch
        }
      };
      if (payload.is_procurement) {
        config.params.product_type = 1
      }
      try {
        const response = await api.get('/products/', config);
        commit('SET_PRODUCTS_FOR_SEARCH', response.data);
        return response
      } catch (e) {
        return e.response
      }
    },
  },
  getters: {
    categoriesForSearch: ({categoriesForSearch}) => categoriesForSearch,
    productsForSearch: ({productsForSearch}) => productsForSearch,
  }
};
